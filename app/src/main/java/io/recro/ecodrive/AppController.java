package io.recro.ecodrive;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.recro.ecodrive.utilities.EcoDrivesMigration;

public class AppController extends MultiDexApplication {
	@Override public void onCreate () {
		super.onCreate ();
		MultiDex.install ( AppController.this );
		configureRealm ();
	}

	private void configureRealm () {
		Realm.init ( AppController.this );
		RealmConfiguration configuration =
				new RealmConfiguration.Builder()
						.name ( "ecodrives.realm" )
						.schemaVersion ( 0 )
						.migration ( new EcoDrivesMigration () )
						.build ();
		/*Realm.deleteRealm ( configuration ); // This will delete Realm existing on the device.*/
		Realm.setDefaultConfiguration ( configuration );
	}

	@Override protected void attachBaseContext ( Context base ) {
		super.attachBaseContext ( base );
	}
}