package io.recro.ecodrive.models;

import io.realm.RealmObject;

/** Represents Location of a Point by having coordinates saved onto it.*/
public class LatLng extends RealmObject {
	/*The Default Value for Latitude and Longitude is set as 9999.9999*/
	private double latitude = 9999.9999;
	private double longitude = 9999.9999;

	public LatLng () {
		this.latitude = 9999.9999;
		this.longitude = 9999.9999;
	}

	public LatLng ( double latitude, double longitude ) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getLatitude () { return latitude; }
	public void setLatitude ( double latitude ) { this.latitude = latitude; }

	public double getLongitude () { return longitude; }
	public void setLongitude ( double longitude ) { this.longitude = longitude; }

	@Override public String toString () {
		return "LatLng { " +
				"latitude = " + latitude +
				", longitude = " + longitude + " }";
	}
}