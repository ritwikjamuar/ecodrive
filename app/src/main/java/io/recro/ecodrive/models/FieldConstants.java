package io.recro.ecodrive.models;

public interface FieldConstants {
	String USER_ID = "id";
	String USER_NAME = "userName";
	String BIKE_ID = "bikeId";
	String MOBILE_NUMBER = "mobileNumber";
	String PASSWORD = "password";
	String IS_LOGGED_IN = "isLoggedIn";
}