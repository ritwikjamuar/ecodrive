package io.recro.ecodrive.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject {
	@PrimaryKey private String id;
	private String userName;
	private String bikeId;
	private String mobileNumber;
	private String password;
	private RealmList<Trip> trip;
	private boolean isLoggedIn;

	public String getId () { return id; }
	public void setId ( String id ) { this.id = id; }

	public String getUserName () { return userName; }
	public void setUserName ( String userName ) { this.userName = userName; }

	public String getBikeId () { return bikeId; }
	public void setBikeId ( String bikeId ) { this.bikeId = bikeId; }

	public String getMobileNumber () { return mobileNumber; }
	public void setMobileNumber ( String mobileNumber ) { this.mobileNumber = mobileNumber; }

	public String getPassword () { return password; }
	public void setPassword ( String password ) { this.password = password; }

	public RealmList<Trip> getTrip () { return trip; }
	public void setTrip ( RealmList<Trip> trip ) { this.trip = trip; }

	public boolean isLoggedIn () { return isLoggedIn; }
	public void setLoggedIn ( boolean loggedIn ) { isLoggedIn = loggedIn; }

	@Override public String toString () {
		return "User { " +
				"id = " + id +
				", userName = " + userName +
				", bikeId = " + bikeId +
				", mobileNumber = " + mobileNumber +
				", password" + password +
				" }";
	}
}