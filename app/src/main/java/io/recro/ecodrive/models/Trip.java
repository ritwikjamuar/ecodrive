package io.recro.ecodrive.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Trip extends RealmObject {
	@PrimaryKey private String id;
	private long startTime = 0;
	private double distanceSaved = 0.0;
	private boolean isTripStarted = false;
	private LatLng tripStartPosition;
	private LatLng tripLastKnownPosition;

	public String getId () { return id; }
	public void setId ( String id ) { this.id = id; }

	public long getStartTime () { return startTime; }
	public void setStartTime ( long startTime ) { this.startTime = startTime; }

	public double getDistanceSaved () { return distanceSaved; }
	public void setDistanceSaved ( double distanceSaved ) { this.distanceSaved = distanceSaved; }

	public boolean isTripStarted () { return isTripStarted; }
	public void setTripStarted ( boolean tripStarted ) { isTripStarted = tripStarted; }

	public LatLng getTripStartPosition () { return tripStartPosition; }
	public void setTripStartPosition ( LatLng tripStartPosition ) { this.tripStartPosition = tripStartPosition; }

	public LatLng getTripLastKnownPosition () { return tripLastKnownPosition; }
	public void setTripLastKnownPosition ( LatLng tripLastKnownPosition ) { this.tripLastKnownPosition = tripLastKnownPosition; }

	@Override public String toString () {
		String strStartTime = "startTime = " + startTime;
		String strDistanceSaved = "distanceSaved = " + distanceSaved;
		String strIsTripStarted = "isTripStarted" + isTripStarted;
		String strTripStartPosition = "null";
		if ( tripStartPosition != null ) strTripStartPosition = "tripStartPosition = " + tripStartPosition.toString ();
		String strTripLaskKnownPosition = "null";
		if ( tripLastKnownPosition != null ) strTripLaskKnownPosition = "tripLastKnownPosition = " + tripLastKnownPosition.toString ();

		return "Trip { "
				+ strStartTime
				+ ", " + strDistanceSaved
				+ ", " + strIsTripStarted
				+ ", " + strTripStartPosition
				+ ", " + strTripLaskKnownPosition
				+ " }";
	}
}