package io.recro.ecodrive.utilities.infoDialog;

import android.content.Context;
import android.support.v7.app.AlertDialog;

public class InfoDialog {
	private static AlertDialog dialog;

	/**
	 * Method to show dialog with out call back
	 *
	 * @param context    Context
	 * @param title      String
	 * @param message    String
	 * @param buttonType ButtonType
	 */
	public static void showDialog ( Context context, String title, String message, ButtonType buttonType ) {
		showDialog ( context, title, message, buttonType, null, null );
	}

	/**
	 * Method to show dialog with call back on button actions using onInfoAction interface
	 *
	 * @param context           Context
	 * @param title             String
	 * @param message           String
	 * @param buttonType        ButtonType
	 * @param onInfoAction      OnInfoAction
	 * @param callBackReference String
	 */
	public static void showDialog ( Context context, String title, String message, ButtonType buttonType, OnInfoAction onInfoAction, String callBackReference ) {
		showDialog ( context, title, message, buttonType, null, null, onInfoAction, callBackReference );
	}

	/**
	 * Method to show dialog with call back on button actions using onInfoAction interface
	 *
	 * @param context           Context
	 * @param title             String
	 * @param message           String
	 * @param buttonType        ButtonType
	 * @param positiveAction    String
	 * @param negativeAction    String
	 * @param onInfoAction      OnInfoAction
	 * @param callBackReference String
	 */
	public static void showDialog(Context context, String title, String message, ButtonType buttonType, String positiveAction, String negativeAction, OnInfoAction onInfoAction, String callBackReference) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(false);
		builder.setTitle(title);
		builder.setMessage(message);
		setButton(context, builder, buttonType, positiveAction, negativeAction, onInfoAction, callBackReference);
		try { dialog.show(); }
		catch ( Exception e ) { e.printStackTrace (); }
	}

	/**
	 * Method to set button for dialog
	 *
	 * @param context           Context
	 * @param builder            AlertDialog.Builder
	 * @param buttonType        ButtonType
	 * @param positiveAction    String
	 * @param negativeAction    String
	 * @param onInfoAction      onInfoAction
	 * @param callBackReference String
	 */
	private static void setButton (
			Context context,
			AlertDialog.Builder builder,
			ButtonType buttonType,
			String positiveAction,
			String negativeAction,
			OnInfoAction onInfoAction,
			String callBackReference ) {
		DialogClickListener dialogClickListener = new DialogClickListener(onInfoAction, callBackReference);
		switch (buttonType) {
			case OK_CANCEL :
				builder.setPositiveButton(context.getResources().getString(android.R.string.ok), dialogClickListener);
				builder.setNegativeButton(context.getResources().getString(android.R.string.cancel), dialogClickListener);
				dialog = builder.create ();
				break;
			case YES_NO :
				builder.setPositiveButton(context.getResources().getString(android.R.string.yes), dialogClickListener);
				builder.setNegativeButton(context.getResources().getString(android.R.string.no), dialogClickListener);
				dialog = builder.create ();
				break;
			case CUSTOM :
				builder.setPositiveButton(positiveAction, dialogClickListener);
				builder.setNegativeButton(negativeAction, dialogClickListener);
				dialog = builder.create ();
				break;
			default :
				builder.setPositiveButton(context.getResources().getString(android.R.string.ok), dialogClickListener);
				dialog = builder.create ();
				break;
		}
	}

	public static void dismiss() {
		if ( dialog != null )
			dialog.dismiss ();
	}

	public static boolean isDialogShown () {
		boolean isShown = false;
		if ( dialog != null ) {
			if ( dialog.isShowing () )
				isShown = true;
			/*else android.util.Log.e ( "Dialog", "Not Showing" );*/
		} /*android.util.Log.e ( "Dialog", "null" );*/
		return isShown;
	}
}