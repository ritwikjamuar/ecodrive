package io.recro.ecodrive.utilities.infoDialog;

public interface OnInfoAction {
	void onInfoActionClick ( InfoAction infoAction, String callBackReference );
}