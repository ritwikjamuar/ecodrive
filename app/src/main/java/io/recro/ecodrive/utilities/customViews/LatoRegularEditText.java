package io.recro.ecodrive.utilities.customViews;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

public class LatoRegularEditText extends EditText {
	public LatoRegularEditText ( Context context ) {
		super ( context );
		initialize ();
	}

	public LatoRegularEditText ( Context context, AttributeSet attrs ) {
		super ( context, attrs );
		initialize ();
	}

	public LatoRegularEditText ( Context context, AttributeSet attrs, int defStyleAttr ) {
		super ( context, attrs, defStyleAttr );
		initialize ();
	}

	@TargetApi (Build.VERSION_CODES.LOLLIPOP)
	public LatoRegularEditText ( Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes ) {
		super ( context, attrs, defStyleAttr, defStyleRes );
	}

	private void initialize () {
		String fontName = "lato-regular.ttf";
		try {
			Typeface myTypeface = Typeface.createFromAsset ( getContext().getAssets(), "fonts/" + fontName );
			setTypeface ( myTypeface );
		} catch ( Exception e ) { e.printStackTrace (); }
	}
}