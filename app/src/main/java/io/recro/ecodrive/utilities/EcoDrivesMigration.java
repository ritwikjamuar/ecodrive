package io.recro.ecodrive.utilities;

import android.support.annotation.NonNull;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;

public class EcoDrivesMigration implements RealmMigration {
	@Override public void migrate ( @NonNull DynamicRealm realm, long oldVersion, long newVersion ) {
		 android.util.Log.e ( "oldVersion", String.valueOf ( oldVersion ) );
		 android.util.Log.e ( "newVersion", String.valueOf ( newVersion ) );
	}
}