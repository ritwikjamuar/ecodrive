package io.recro.ecodrive.utilities.customViews;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/** Text View with Lato Regular Font. */
public class LatoRegularTextView extends TextView {
	public LatoRegularTextView ( Context context ) {
		super ( context );
		initialize ();
	}

	public LatoRegularTextView ( Context context, @Nullable AttributeSet attrs ) {
		super ( context, attrs );
		initialize ();
	}

	public LatoRegularTextView ( Context context, @Nullable AttributeSet attrs, int defStyleAttr ) {
		super ( context, attrs, defStyleAttr );
		initialize ();
	}

	@TargetApi ( Build.VERSION_CODES.LOLLIPOP )
	public LatoRegularTextView ( Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes ) {
		super ( context, attrs, defStyleAttr, defStyleRes );
		initialize ();
	}

	/** Method to Initialize the TextView and apply the Font. */
	private void initialize () {
		String fontName = "lato-regular.ttf";
		try {
			Typeface myTypeface = Typeface.createFromAsset ( getContext().getAssets(), "fonts/" + fontName );
			setTypeface ( myTypeface );
		} catch ( Exception e ) { e.printStackTrace (); }
	}
}