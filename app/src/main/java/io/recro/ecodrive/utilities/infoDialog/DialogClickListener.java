package io.recro.ecodrive.utilities.infoDialog;

import android.content.DialogInterface;

class DialogClickListener implements DialogInterface.OnClickListener {
	private String callBackReference;
	private OnInfoAction onInfoAction;

	public DialogClickListener(OnInfoAction onInfoAction, String callBackReference) {
		this.callBackReference = callBackReference;
		this.onInfoAction = onInfoAction;
	}

	@Override public void onClick ( DialogInterface dialog, int which ) {
		dialog.dismiss ();
		if ( onInfoAction != null ) {
			InfoAction infoAction = InfoAction.POSITIVE;
			if ( which == DialogInterface.BUTTON_NEGATIVE )
				infoAction = InfoAction.NEGATIVE;
			onInfoAction.onInfoActionClick(infoAction, callBackReference);
		}
	}
}