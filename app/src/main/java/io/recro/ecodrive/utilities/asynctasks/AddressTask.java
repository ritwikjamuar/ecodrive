package io.recro.ecodrive.utilities.asynctasks;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import io.recro.ecodrive.models.LatLng;

public class AddressTask extends AsyncTask <LatLng, Void, Void > {
	private Geocoder geocoder;
	private List<Address> addresses;
	private String address;
	private Context mContext;
	private AddressListener mListener;

	public AddressTask ( Context mContext, AddressListener mListener ) {
		this.mContext = mContext;
		this.mListener = mListener;
	}

	@Override protected void onPreExecute () {
		super.onPreExecute ();
		initializeItems ();
	}

	/**Initializes components required to get the Location.
	 * Also, starts the Progress Dialog.*/
	private void initializeItems () {
		geocoder = new Geocoder ( mContext );
		addresses = new ArrayList<> ();
		address = "";
	}

	@Override protected Void doInBackground ( LatLng... params ) {
		LatLng point = params [ 0 ];
		fetchAddress ( point.getLatitude (), point.getLongitude () );
		return null;
	}

	/**Performs the task of obtaining Address as String.
	 * @param latitude Latitude of the marker point.
	 * @param longitude Longitude of the marker point.*/
	private void fetchAddress ( double latitude, double longitude ) {
		try {
			// Get List of Address using Geo Coder (Google API).
			addresses = geocoder.getFromLocation ( latitude, longitude, 5 );
			// Initialize StringBuilder to store concatenated results.
			StringBuilder addressBuilder = new StringBuilder ( "" );
			// Iterate over list of Addresses.
			for ( Address add : addresses ) {
				// Check whether address contains Postal Code information or not.
				if ( add.getPostalCode () != null ) {
					// Iterate over address lines of address satisfying the condition.
					for ( int i = 0; i < add.getMaxAddressLineIndex (); ++i )
						addressBuilder.append ( add.getAddressLine ( i ) ).append ( "," );
					// Remove ',' at the last of addressBuilder.
					addressBuilder.deleteCharAt ( addressBuilder.lastIndexOf ( "," ) );
					// Convert this StringBuilder to String and assign to the String value.
					address = addressBuilder.toString ();
					break;
				}
			}
		} catch ( Exception e ) { e.printStackTrace (); }
	}

	@Override protected void onPostExecute ( Void aVoid ) {
		super.onPostExecute ( aVoid );
		mContext = null;
		decide ();
	}

	/**Decides which implemented method has to be executed.
	 * Also dismisses the dialog.*/
	private void decide () {
		if ( address.trim ().length () == 0 ) mListener.errorOnFetchingAddress ( "Address Not Found" );
		else mListener.onAddressFetchedSuccess ( address );
	}

	private interface AddressListener {
		void onAddressFetchedSuccess ( String address );
		void errorOnFetchingAddress ( String error );
	}
}