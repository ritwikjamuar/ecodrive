package io.recro.ecodrive.utilities.infoDialog;

public enum ButtonType {
	OK,
	OK_CANCEL,
	YES_NO,
	CUSTOM
}