package io.recro.ecodrive.utilities.customViews.speedometer;

public interface SpeedometerListener {
	void onSpeedChanged ( float newSpeedValue );
}