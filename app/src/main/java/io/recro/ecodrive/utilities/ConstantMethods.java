package io.recro.ecodrive.utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.widget.Toast;

import io.recro.ecodrive.R;
import io.recro.ecodrive.models.LatLng;

public class ConstantMethods {
	private static ProgressDialog progressDialog;

	/**
	 * Shows the Toast Message from any screen.
	 * @param context Context of the given invocation of method.
	 * @param message Message to be displayed as Toast.
	 */
	public static void showToastMessage ( Context context, String message ) {
		Toast.makeText ( context, message, Toast.LENGTH_SHORT ).show ();
	}

	/**
	 * Shows the Toast Message from any screen.
	 * @param context Context of the given invocation of method.
	 * @param resourceID ID of String Message to be displayed as Toast.
	 */
	public static void showToastMessage ( Context context, int resourceID ) {
		Toast.makeText ( context, resourceID, Toast.LENGTH_SHORT ).show ();
	}

	/**
	 * Shows the Progress Dialog.
	 * @param context Context of the given invocation of method.
	 */
	public static void showProgress ( Context context ) {
		if ( Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP ) { progressDialog = new ProgressDialog ( context ); }
		else { progressDialog = new ProgressDialog ( context, R.style.AppTheme_Dialog ); }
		progressDialog.setMessage ( "Loading..." );
		progressDialog.setCancelable ( false );
		try { progressDialog.show (); }
		catch ( Exception e ) { e.printStackTrace (); }
	}

	/**
	 * Checks whether Progress Dialog is showing in the screen or not.
	 *
	 * @return true when Progress Dialog is showing, else false.
	 */
	public static boolean isProgressShown () {
		boolean isShown = false;
		if ( progressDialog != null ) {
			if ( progressDialog.isShowing () ) { isShown = true; }
		}
		return isShown;
	}

	/** Removes the Progress Dialog. */
	public static void removeProgress () {
		if ( progressDialog != null ) {
			if ( progressDialog.isShowing () ) {
				progressDialog.dismiss ();
				progressDialog = null;
			}
		}
	}

	/**
	 * Method to convert dp values to pixel
	 * @param dp float
	 * @param context Context
	 * @return float
	 */
	public static float convertDpToPixel ( float dp, Context context ) {
		Resources resources = context.getResources ();
		DisplayMetrics metrics = resources.getDisplayMetrics ();
		return dp * ( metrics.densityDpi / 160f );
	}

	/**Calculate distance between two points.
	 * Uses Haversine method as its base.
	 * @param source {@link LatLng} of Source Point.
	 * @param destination {@link LatLng} of Destination Point.
	 * @return Distance in Meters*/
	public static double distance ( LatLng source, LatLng destination ) {
		final int R = 6371; // Radius of the earth
		double latDistance = Math.toRadians ( destination.getLatitude () - source.getLatitude () );
		double lonDistance = Math.toRadians ( destination.getLongitude () - source.getLongitude () );
		double a =
				Math.sin ( latDistance / 2 ) * Math.sin ( latDistance / 2 ) +
				Math.cos ( Math.toRadians ( source.getLatitude () ) ) * Math.cos ( Math.toRadians ( destination.getLatitude () ) ) *
				Math.sin ( lonDistance / 2 ) * Math.sin ( lonDistance / 2 );
		double c = 2 * Math.atan2 ( Math.sqrt ( a ), Math.sqrt ( 1 - a ) );
		double distance = R * c * 1000; // convert to meters
		/*double height = el1 - el2;*/
		distance = Math.pow ( distance, 2 ) /*+ Math.pow ( height, 2 )*/;
		return Math.sqrt ( distance );
	}

	/**Converts the Speed Unit from m/s to km/h.
	 * @param speed Speed in m/s.
	 * @return Speed in km/h.*/
	public static float convertSpeed ( float speed ) { return ( ( speed * 18 ) / 5 ); }
}