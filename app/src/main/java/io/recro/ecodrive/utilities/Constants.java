package io.recro.ecodrive.utilities;

/** Contains the commonly used fields. */
public interface Constants {
	String[] DUMMY_CREDENTIALS = new String[] {
			"1234567890:password",
			"9876543210:pass"
	};
	String USER_ID = "userID";
}