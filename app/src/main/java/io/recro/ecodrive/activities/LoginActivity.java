package io.recro.ecodrive.activities;

import android.content.Context;
import android.content.Intent;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

import android.widget.TextView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import io.recro.ecodrive.R;
import io.recro.ecodrive.realmDBTasks.UserLoginTask;
import io.recro.ecodrive.utilities.ConstantMethods;
import io.recro.ecodrive.utilities.Constants;

/** A login screen that offers login via Mobile Number and Password. */
public class LoginActivity
		extends AppCompatActivity
		implements View.OnFocusChangeListener,
		           UserLoginTask.UserLoginListener {
    // Views.
    @BindView ( R.id.activity_login_text_input_layout_mobile_number) TextInputLayout mTILMobileNumber;
	@BindView ( R.id.activity_login_text_input_layout_password ) TextInputLayout mTILPassword;
	@BindView ( R.id.activity_login_text_input_edit_text_mobile_number ) TextInputEditText mTIETMobileNumber;
	@BindView ( R.id.activity_login_text_input_edit_text_password ) TextInputEditText mTIETPassword;

	// String Resources.
	@BindString ( R.string.activity_login_text_mobile_number_caps ) String sMobileNumberCaps;
	@BindString ( R.string.activity_login_text_mobile_number_no_caps ) String sMobileNumberNoCaps;
	@BindString ( R.string.activity_login_text_password_caps ) String sPasswordCaps;
	@BindString ( R.string.activity_login_text_password_no_caps ) String sPasswordNoCaps;
	@BindString ( R.string.activity_login_text_validation_empty_mobile_number ) String sValidationEmptyMobileNumber;
	@BindString ( R.string.activity_login_text_validation_invalid_mobile_number ) String sValidationInvalidMobileNumber;
	@BindString ( R.string.activity_login_text_validation_empty_password ) String sValidationEmptyPassword;

	private UserLoginTask mUserLoginTask;

    @Override protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_login );
	    ButterKnife.bind ( LoginActivity.this );
        initializeViews ();
    }

	/** Initialize the Views and sets the Listeners. */
    private void initializeViews () {
    	// Set Hint to TextInputLayout.
	    mTILMobileNumber.setHint ( sMobileNumberNoCaps );
	    mTILPassword.setHint ( sPasswordNoCaps );

	    // Manage the TextInputEditTexts Focus Animations.
	    mTIETMobileNumber.setOnFocusChangeListener ( LoginActivity.this );
	    mTIETPassword.setOnFocusChangeListener ( LoginActivity.this );

	    // Implement onEditorActionListener to Password Field so that when user clicks on Enter, Login Service shall hit.
	    final View view = this.getCurrentFocus();
	    mTIETPassword.setOnEditorActionListener (
			    new TextView.OnEditorActionListener () {
				    @Override public boolean onEditorAction ( TextView textView, int id, KeyEvent keyEvent ) {
					    if ( id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL ) {
						    if ( view != null ) {
							    InputMethodManager imm = (InputMethodManager) getSystemService ( Context.INPUT_METHOD_SERVICE );
							    if ( imm != null ) imm.hideSoftInputFromWindow ( view.getWindowToken(), 0 );
						    }
					    	attemptLogin();
						    return true;
					    } return false;
				    }
			    }
	    );
    }

	@Override protected void onStop () {
		super.onStop ();
		if ( mUserLoginTask != null && mUserLoginTask.isTransactionOnGoing () ) mUserLoginTask.cancelTransaction ();
	}

	@Override public void onFocusChange ( View view, boolean hasFocus ) {
    	switch ( view.getId () ) {
		    case R.id.activity_login_text_input_edit_text_mobile_number:
		    	if ( hasFocus ) mTILMobileNumber.setHint ( sMobileNumberCaps );
			    else if ( mTIETMobileNumber.getText ().toString ().isEmpty () ) mTILMobileNumber.setHint ( sMobileNumberNoCaps );
		    	break;
		    case R.id.activity_login_text_input_edit_text_password :
		    	if ( hasFocus ) mTILPassword.setHint ( sPasswordCaps );
		    	else if ( mTIETPassword.getText ().toString ().isEmpty () ) mTILPassword.setHint ( sPasswordNoCaps );
		    	break;
	    }
	}

	@OnClick ( R.id.activity_login_text_view_forgot_password ) void forgotPassword () { startForgotPasswordActivity (); }

	@OnClick ( R.id.activity_login_text_view_sign_up ) void register () { startRegisterActivity (); }

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	@OnClick ( R.id.activity_login_relative_layout_sign_in ) void attemptLogin () {
		if ( checkValidity () ) {
			// Reset the Errors.
			mTIETMobileNumber.setError ( null );
			mTIETPassword.setError ( null );

			// Store the values at the Time of Login Attempt.
			String mobileNumber = mTIETMobileNumber.getText ().toString ();
			String password = mTIETPassword.getText ().toString ();

			// Kick off a background task to perform the user login attempt.
			ConstantMethods.showProgress ( LoginActivity.this );
			mUserLoginTask = new UserLoginTask ( LoginActivity.this );
			mUserLoginTask.setFields ( mobileNumber, password );
			mUserLoginTask.execute ();
		}
	}

	/**
	 * Validates the Input from the User.
	 * @return True, if all the fields are valid for attempting Login, else False.
	 */
	private boolean checkValidity () {
		boolean isValid = false;
		if ( mTIETMobileNumber.getText ().toString ().isEmpty () ) {
			mTIETMobileNumber.setError ( sValidationEmptyMobileNumber );
			mTILMobileNumber.requestFocus ();
		} else if ( mTIETMobileNumber.getText ().toString ().length () != 10 ) {
			mTIETMobileNumber.setError ( sValidationInvalidMobileNumber );
			mTILMobileNumber.requestFocus ();
		} else if ( mTIETPassword.getText ().toString ().isEmpty () ) {
			mTIETPassword.setError ( sValidationEmptyPassword );
			mTILPassword.requestFocus ();
		} else isValid = true;
		return isValid;
	}

	private void startForgotPasswordActivity () {
		Intent intent = new Intent ( LoginActivity.this, ForgotPasswordActivity.class );
		startActivity ( intent );
	}

	private void startRegisterActivity () {
		Intent intent = new Intent ( LoginActivity.this, RegisterActivity.class );
		startActivity ( intent );
		finish ();
	}

	private void startHomeActivity ( String userID ) {
		Intent intent = new Intent ( LoginActivity.this, MainActivity.class );
		intent.putExtra ( Constants.USER_ID, userID );
		startActivity ( intent );
		finish ();
	}

	@Override public void onLoginSuccess ( String userID ) {
		if ( ConstantMethods.isProgressShown () ) ConstantMethods.removeProgress ();
		ConstantMethods.showToastMessage ( LoginActivity.this, "Success" );
		startHomeActivity ( userID );
	}

	@Override public void onLoginFailure ( final String errorMessage ) {
		runOnUiThread (
				new Runnable () {
					@Override public void run () {
						if ( ConstantMethods.isProgressShown () ) ConstantMethods.removeProgress ();
						ConstantMethods.showToastMessage ( LoginActivity.this, errorMessage );
					}
				}
		);
	}
}