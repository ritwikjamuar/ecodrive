package io.recro.ecodrive.activities;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;

import android.text.TextUtils;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import io.recro.ecodrive.R;
import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.realmDBTasks.ChangePasswordTask;
import io.recro.ecodrive.utilities.ConstantMethods;

public class ChangePasswordActivity
		extends AppCompatActivity
		implements View.OnFocusChangeListener,
		           ChangePasswordTask.ChangePasswordListener {
	// Views.
	@BindView ( R.id.activity_change_password_text_input_layout_new_password ) TextInputLayout mTILNewPassword;
	@BindView ( R.id.activity_change_password_text_input_edit_text_new_password ) TextInputEditText mTIETNewPassword;
	@BindView ( R.id.activity_change_password_text_input_layout_confirm_password ) TextInputLayout mTILConfirmPassword;
	@BindView ( R.id.activity_change_password_text_input_edit_text_confirm_password ) TextInputEditText mTIETConfirmPassword;

	// String Resources.
	@BindString ( R.string.activity_change_password_text_new_password_caps ) String sNewPasswordCaps;
	@BindString ( R.string.activity_change_password_text_new_password_no_caps ) String sNewPasswordNoCaps;
	@BindString ( R.string.activity_change_password_text_confirm_password_caps ) String sConfirmPasswordCaps;
	@BindString ( R.string.activity_change_password_text_confirm_password_no_caps ) String sConfirmPasswordNoCaps;
	@BindString ( R.string.activity_change_password_text_validation_empty_password ) String sEmptyPassword;
	@BindString ( R.string.activity_change_password_text_validation_password_inadequate_length ) String sPasswordLessLength;
	@BindString ( R.string.activity_change_password_text_validation_password_do_not_match ) String sPasswordMismatch;

	// Realm DB Tasks.
	private ChangePasswordTask mTask;

	// Variables.
	private String mUserID;

	@Override protected void onCreate ( Bundle savedInstanceState ) {
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_change_password );
		ButterKnife.bind ( ChangePasswordActivity.this );
		initializeComponents ();
		getDataFromPreviousActivity ();
		initializeViews ();
	}

	/** Initializes the Other Components of this Activity. */
	private void initializeComponents () {
		mTask = new ChangePasswordTask ( ChangePasswordActivity.this );
	}

	/** Gets User ID from {@link ForgotPasswordActivity} */
	private void getDataFromPreviousActivity () {
		Bundle bundle = getIntent ().getExtras ();
		if ( bundle == null ) {
			ConstantMethods.showToastMessage ( ChangePasswordActivity.this, "Error on Getting Data from Previous Screen" );
			finish ();
		} else mUserID = bundle.getString ( FieldConstants.USER_ID );
	}

	/** Initializes the Views of this Activity. */
	private void initializeViews () {
		// Set Hint to TextInputLayout.
		mTILNewPassword.setHint ( sNewPasswordNoCaps );
		mTILConfirmPassword.setHint ( sConfirmPasswordNoCaps );

		// Manage the TextInputEditTexts Focus Animations.
		mTIETNewPassword.setOnFocusChangeListener ( ChangePasswordActivity.this );
		mTIETConfirmPassword.setOnFocusChangeListener ( ChangePasswordActivity.this );

		// Implement onEditorActionListener to Password Field so that when user clicks on Enter, Login Service shall hit.
		final View view = this.getCurrentFocus();
		mTIETConfirmPassword.setOnEditorActionListener (
				new TextView.OnEditorActionListener () {
					@Override public boolean onEditorAction ( TextView textView, int id, KeyEvent keyEvent ) {
						if ( id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL ) {
							if ( view != null ) {
								InputMethodManager imm = (InputMethodManager) getSystemService ( Context.INPUT_METHOD_SERVICE );
								if ( imm != null ) imm.hideSoftInputFromWindow ( view.getWindowToken(), 0 );
							}
							changePassword ();
							return true;
						} return false;
					}
				}
		);
	}

	@Override protected void onStop () {
		super.onStop ();
		if ( mTask != null && mTask.isInTransaction () ) mTask.cancelTransaction ();
	}

	@Override public void onFocusChange ( View view, boolean hasFocus ) {
		switch ( view.getId () ) {
			case R.id.activity_change_password_text_input_edit_text_new_password : {
				if ( hasFocus ) mTILNewPassword.setHint ( sNewPasswordCaps );
				else if ( mTIETNewPassword.getText ().toString ().isEmpty () ) mTILNewPassword.setHint ( sNewPasswordNoCaps );
			} break;

			case R.id.activity_change_password_text_input_edit_text_confirm_password : {
				if ( hasFocus ) mTILConfirmPassword.setHint ( sConfirmPasswordCaps );
				else if ( mTIETConfirmPassword.getText ().toString ().isEmpty () ) mTILConfirmPassword.setHint ( sConfirmPasswordNoCaps );
			} break;
		}
	}

	@OnClick ( R.id.activity_change_password_relative_layout_change_password ) void changePassword () {
		if ( validate () ) {
			ConstantMethods.showProgress ( ChangePasswordActivity.this );
			mTask.setData ( mUserID, mTIETNewPassword.getText ().toString () );
			mTask.execute ();
		}
	}

	/**Validates the Inputs of user.
	 * In this, passwords will be matched as well as checked for preferred length. */
	private boolean validate () {
		boolean isValid = false;
		if ( TextUtils.isEmpty ( mTIETNewPassword.getText ().toString () ) ) {
			mTIETNewPassword.setError ( sEmptyPassword );
			mTILNewPassword.requestFocus ();
		} else if ( mTIETNewPassword.getText ().toString ().length () < 5 ) {
			mTIETNewPassword.setError ( sPasswordLessLength );
			mTILNewPassword.requestFocus ();
		} else if ( TextUtils.isEmpty ( mTIETConfirmPassword.getText ().toString () ) ) {
			mTIETConfirmPassword.setError ( sEmptyPassword );
			mTILConfirmPassword.requestFocus ();
		} else if ( ! mTIETNewPassword.getText ().toString ().equals ( mTIETConfirmPassword.getText ().toString () ) ) {
			mTIETConfirmPassword.setError ( sPasswordMismatch );
			mTILConfirmPassword.requestFocus ();
		} else isValid = true;
		return isValid;
	}

	@Override public void onPasswordChangedSuccessfully () {
		ConstantMethods.removeProgress ();
		ConstantMethods.showToastMessage ( ChangePasswordActivity.this, "Password Changed Successfully" );
		finish ();
	}

	@Override public void errorOnChangingPassword ( final String error ) {
		runOnUiThread (
				new Runnable () {
					@Override public void run () {
						ConstantMethods.removeProgress ();
						ConstantMethods.showToastMessage ( ChangePasswordActivity.this, error );
					}
				}
		);
	}
}