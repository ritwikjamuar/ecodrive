package io.recro.ecodrive.activities;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;

import android.text.TextUtils;

import android.view.View;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import io.recro.ecodrive.R;
import io.recro.ecodrive.realmDBTasks.RegisterUserTask;
import io.recro.ecodrive.utilities.ConstantMethods;
import io.recro.ecodrive.utilities.Constants;

public class RegisterActivity
		extends AppCompatActivity
		implements View.OnFocusChangeListener,
		           RegisterUserTask.RegisterUserListener {
    // Views
	@BindView ( R.id.activity_register_text_input_layout_full_name ) TextInputLayout mTILFullName;
    @BindView ( R.id.activity_register_text_input_edit_text_full_name ) TextInputEditText mTIETFullName;
    @BindView ( R.id.activity_register_text_input_layout_bike_id ) TextInputLayout mTILBikeID;
    @BindView ( R.id.activity_register_text_input_edit_text_bike_id ) TextInputEditText mTIETBikeID;
    @BindView ( R.id.activity_register_text_input_layout_mobile_number ) TextInputLayout mTILMobileNumber;
    @BindView ( R.id.activity_register_text_input_edit_text_mobile_number ) TextInputEditText mTIETMobileNumber;
    @BindView ( R.id.activity_register_text_input_layout_password ) TextInputLayout mTILPassword;
    @BindView ( R.id.activity_register_text_input_edit_text_password ) TextInputEditText mTIETPassword;
    @BindView ( R.id.activity_register_text_input_layout_confirm_password ) TextInputLayout mTILConfirmPassword;
    @BindView ( R.id.activity_register_text_input_edit_text_confirm_password ) TextInputEditText mTIETConfirmPassword;

    // String Resources
	@BindString ( R.string.activity_register_text_full_name_caps ) String sFullNameCaps;
	@BindString ( R.string.activity_register_text_full_name_no_caps ) String sFullNameNoCaps;
	@BindString ( R.string.activity_register_text_bike_id_caps ) String sBikeIDCaps;
	@BindString ( R.string.activity_register_text_bike_id_no_caps ) String sBikeIDNoCaps;
	@BindString ( R.string.activity_register_text_mobile_number_caps ) String sMobileNumberCaps;
	@BindString ( R.string.activity_register_text_mobile_number_no_caps ) String sMobileNumberNoCaps;
	@BindString ( R.string.activity_register_text_password_caps ) String sPasswordCaps;
	@BindString ( R.string.activity_register_text_password_no_caps ) String sPasswordNoCaps;
	@BindString ( R.string.activity_register_text_confirm_password_caps ) String sConfirmPasswordCaps;
	@BindString ( R.string.activity_register_text_confirm_password_no_caps ) String sConfirmPasswordNoCaps;
	@BindString ( R.string.activity_register_text_validation_empty_full_name ) String sEmptyFullName;
	@BindString ( R.string.activity_register_text_validation_empty_bike_id ) String sEmptyBikeID;
	@BindString ( R.string.activity_register_text_validation_empty_mobile_number ) String sEmptyMobileNumber;
	@BindString ( R.string.activity_register_text_validation_invalid_mobile_number ) String sInvalidMobileNumber;
	@BindString ( R.string.activity_register_text_validation_empty_password ) String sEmptyPassword;
	@BindString ( R.string.activity_register_text_validation_password_inadequate_length ) String sPasswordLessLength;
	@BindString ( R.string.activity_register_text_validation_password_do_not_match ) String sPasswordMismatch;

	// Realm DB Tasks.
	private RegisterUserTask mTask;

	@Override protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_register );
		ButterKnife.bind ( RegisterActivity.this );
		initializeComponents ();
		initializeViews ();
    }

    private void initializeComponents () {
		mTask = new RegisterUserTask ( RegisterActivity.this );
    }

	private void initializeViews () {
		// Set Hint to TextInputLayout;
	    mTILFullName.setHint ( sFullNameNoCaps );
	    mTILBikeID.setHint ( sBikeIDNoCaps );
	    mTILMobileNumber.setHint ( sMobileNumberNoCaps );
	    mTILPassword.setHint ( sPasswordNoCaps );
	    mTILConfirmPassword.setHint ( sConfirmPasswordNoCaps );

	    // Manage the TextInputEditTexts Focus Animations.
	    mTIETFullName.setOnFocusChangeListener ( RegisterActivity.this );
	    mTIETBikeID.setOnFocusChangeListener ( RegisterActivity.this );
	    mTIETMobileNumber.setOnFocusChangeListener ( RegisterActivity.this );
	    mTIETPassword.setOnFocusChangeListener ( RegisterActivity.this );
	    mTIETConfirmPassword.setOnFocusChangeListener ( RegisterActivity.this );
    }

	@Override protected void onStop () {
		super.onStop ();
		if ( mTask != null && mTask.isTransactionOnGoing () ) mTask.cancelTransaction ();
	}

	@Override public void onFocusChange ( View view, boolean hasFocus ) {
		switch ( view.getId () ) {
			case R.id.activity_register_text_input_edit_text_full_name : {
				if ( hasFocus ) { mTILFullName.setHint ( sFullNameCaps ); }
				else if ( mTIETFullName.getText ().toString ().isEmpty () ) mTILFullName.setHint ( sFullNameNoCaps );
			} break;

			case R.id.activity_register_text_input_edit_text_bike_id : {
				if ( hasFocus ) { mTILBikeID.setHint ( sBikeIDCaps ); }
				else if ( mTIETBikeID.getText ().toString ().isEmpty () ) mTILBikeID.setHint ( sBikeIDNoCaps );
			} break;

			case R.id.activity_register_text_input_edit_text_mobile_number : {
				if ( hasFocus ) { mTILMobileNumber.setHint ( sMobileNumberCaps ); }
				else if ( mTIETMobileNumber.getText ().toString ().isEmpty () ) mTILMobileNumber.setHint ( sMobileNumberNoCaps );
			} break;

			case R.id.activity_register_text_input_edit_text_password : {
				if ( hasFocus ) { mTILPassword.setHint ( sPasswordCaps ); }
				else if ( mTIETPassword.getText ().toString ().isEmpty () ) mTILPassword.setHint ( sPasswordNoCaps );
			} break;

			case R.id.activity_register_text_input_edit_text_confirm_password : {
				if ( hasFocus ) { mTILConfirmPassword.setHint ( sConfirmPasswordCaps ); }
				else if ( mTIETConfirmPassword.getText ().toString ().isEmpty () ) mTILConfirmPassword.setHint ( sConfirmPasswordNoCaps );
			} break;
		}
	}

	@OnClick ( R.id.activity_register_relative_layout_sign_up ) void attemptRegister () {
		if ( checkValidity () ) {
			removeErrorFromEditTexts ();
			ConstantMethods.showProgress ( RegisterActivity.this );
			mTask.setFields (
					mTIETFullName.getText ().toString (),
			        mTIETBikeID.getText ().toString (),
			        mTIETMobileNumber.getText ().toString (),
			        mTIETPassword.getText ().toString ()
			);
			mTask.execute ();
		}
	}

	@OnClick ( R.id.activity_register_text_view_sign_in ) void login () { startLoginActivity (); }

	private void removeErrorFromEditTexts () {
		mTIETFullName.setError ( null );
		mTIETBikeID.setError ( null );
		mTIETMobileNumber.setError ( null );
		mTIETPassword.setError ( null );
		mTIETConfirmPassword.setError ( null );
	}

	private boolean checkValidity () {
		boolean isValid = false;
		if ( TextUtils.isEmpty ( mTIETFullName.getText ().toString () ) ) {
			mTIETFullName.setError ( sEmptyFullName );
			mTILFullName.requestFocus ();
		} else if ( TextUtils.isEmpty ( mTIETBikeID.getText ().toString () ) ) {
			mTIETBikeID.setError ( sEmptyBikeID );
			mTILBikeID.requestFocus ();
		} else if ( TextUtils.isEmpty ( mTIETMobileNumber.getText ().toString () ) ) {
			mTIETMobileNumber.setError ( sEmptyMobileNumber );
			mTILMobileNumber.requestFocus ();
		} else if ( mTIETMobileNumber.getText ().toString ().length () != 10 ) {
			mTIETMobileNumber.setError ( sInvalidMobileNumber );
			mTILMobileNumber.requestFocus ();
		} else if ( TextUtils.isEmpty ( mTIETPassword.getText ().toString () ) ) {
			mTIETPassword.setError ( sEmptyPassword );
			mTILPassword.requestFocus ();
		} else if ( mTIETPassword.getText ().toString ().length () < 5 ) {
			mTIETPassword.setError ( sPasswordLessLength );
			mTILPassword.requestFocus ();
		} else if ( TextUtils.isEmpty ( mTIETConfirmPassword.getText ().toString () ) ) {
			mTIETConfirmPassword.setError ( sEmptyPassword );
			mTILConfirmPassword.requestFocus ();
		} else if ( ! mTIETPassword.getText ().toString ().equals ( mTIETConfirmPassword.getText ().toString () ) ) {
			mTIETConfirmPassword.setError ( sPasswordMismatch );
			mTILConfirmPassword.requestFocus ();
		} else isValid = true;
		return isValid;
	}

	@Override public void onUserRegistered ( String userID ) {
		ConstantMethods.removeProgress ();
		startMainActivity ( userID );
	}

	@Override public void onUserAlreadyFound () {
		ConstantMethods.removeProgress ();
		ConstantMethods.showToastMessage ( RegisterActivity.this, "User already Registered" );
	}

	@Override public void errorOnRegistration ( String error ) {
		ConstantMethods.removeProgress ();
		ConstantMethods.showToastMessage ( RegisterActivity.this, error );
	}

	private void startMainActivity ( String userID ) {
		Intent intent = new Intent ( RegisterActivity.this, MainActivity.class );
		intent.putExtra ( Constants.USER_ID, userID );
		startActivity ( intent );
		finish ();
	}

	private void startLoginActivity () {
		Intent intent = new Intent ( RegisterActivity.this, LoginActivity.class );
		startActivity ( intent );
		finish ();
	}
}