package io.recro.ecodrive.activities;

import android.content.Context;
import android.content.Intent;

import android.graphics.Color;
import android.graphics.Typeface;

import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;

import android.text.Spannable;
import android.text.SpannableString;

import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import io.realm.RealmList;
import io.recro.ecodrive.R;

import io.recro.ecodrive.fragments.ContributionFragment;
import io.recro.ecodrive.fragments.HelpFragment;
import io.recro.ecodrive.fragments.HomeFragment;

import io.recro.ecodrive.fragments.NavigateFragment;
import io.recro.ecodrive.fragments.TrackFragment;
import io.recro.ecodrive.models.LatLng;
import io.recro.ecodrive.models.Trip;
import io.recro.ecodrive.models.User;

import io.recro.ecodrive.realmDBTasks.GetAllTripsTask;
import io.recro.ecodrive.realmDBTasks.GetUserTask;
import io.recro.ecodrive.realmDBTasks.LogOutUserTask;
import io.recro.ecodrive.realmDBTasks.StartCyclingTask;
import io.recro.ecodrive.realmDBTasks.StopCyclingTask;
import io.recro.ecodrive.realmDBTasks.UpdateCyclingTask;

import io.recro.ecodrive.utilities.ConstantMethods;
import io.recro.ecodrive.utilities.Constants;
import io.recro.ecodrive.utilities.GPSLocation;
import io.recro.ecodrive.utilities.customViews.CustomTypefaceSpan;

public class MainActivity
		extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener,
		           HomeFragment.HomeListener,
		           ContributionFragment.ContributionListener,
		           HelpFragment.HelpListener,
		           LogOutUserTask.LogOutUserListener,
		           GetUserTask.GetUserListener,
		           GPSLocation.LocationListener,
		           StartCyclingTask.StartCyclingListener,
		           StopCyclingTask.StopCyclingListener,
		           UpdateCyclingTask.UpdateCyclingListener,
		           GetAllTripsTask.GetAllTripsListener {
	// Views
	@BindView ( R.id.activity_main_navigation_view ) NavigationView mNVHome;
	@BindView ( R.id.activity_main_fragment_content ) FrameLayout mFlFragmentContent;
	@BindView ( R.id.activity_main_toolbar ) Toolbar mToolbar;
	@BindView ( R.id.activity_main_drawer_layout ) DrawerLayout mDrawerLayout;
	private TextView mTvUserName;

	// Other Components
	private FragmentManager mFragmentManager;
	private GPSLocation mLocation;

	// Fragments.
	private HomeFragment mHomeFragment;
	private ContributionFragment mContributionFragment;
	private HelpFragment mHelpFragment;
	private NavigateFragment mNavigateFragment;
	private TrackFragment mTrackFragment;

	// Realm DB Tasks.
	private GetUserTask mGetUserTask;
	private LogOutUserTask mLogOutTask;
	private StartCyclingTask mStartCyclingTask;
	private StopCyclingTask mStopCyclingTask;
	private UpdateCyclingTask mUpdateCyclingTask;
	private GetAllTripsTask mGetAllTripsTask;

	// Variables.
	private String mUserID;
	private LatLng mCurrentPosition, mPreviousPosition;
	private double mDistance = 0.0, mTotalDistance = 0.0;
	private User mUser;
	private boolean isTripStarted = false;

	@Override protected void onCreate ( Bundle savedInstanceState ) {
		super.onCreate ( savedInstanceState );
		setContentView( R.layout.activity_main );
		ButterKnife.bind ( MainActivity.this );
		getDataFromPreviousActivity ();
		initializeComponents ();
		initializeActionBar ();
		initializeViews ();
		showHomeFragment ();
		fetchCurrentUser ();
	}

	@Override protected void onPause () {
		super.onPause ();
		mLocation.cancelLocationUpdate ();
	}

	@Override protected void onResume () {
		super.onResume ();
		mLocation.requestLocationUpdate ();
	}

	@Override public void onBackPressed () {
		if ( mDrawerLayout.isDrawerOpen ( Gravity.START ) ) mDrawerLayout.closeDrawer ( Gravity.START );
		else {
			FragmentNames names = getInstanceOfFragment ();
			android.util.Log.e ( "names", names.name () );
			int backStackEntryCount = getSupportFragmentManager ().getBackStackEntryCount ();
			android.util.Log.e ( "backStackEntryCount", String.valueOf ( backStackEntryCount ) );
			if ( backStackEntryCount > 1 ) getSupportFragmentManager ().popBackStack ();
			else {
				if (
						names == FragmentNames.CONTRIBUTION
							||
						names == FragmentNames.HELP
							||
						names == FragmentNames.NAVIGATE
							||
						names == FragmentNames.TRACK
				) {
					mFlFragmentContent.removeAllViews ();
					mNVHome.getMenu ().getItem ( 0 ).setChecked ( true );
				} else {
					super.onBackPressed ();
					finish ();
				}
			}
		}
	}

	@Override public boolean onOptionsItemSelected ( MenuItem item ) {
		switch ( item.getItemId () ) {
			case android.R.id.home : {
				mDrawerLayout.openDrawer ( Gravity.START );
				return true;
			}
		}
		return super.onOptionsItemSelected ( item );
	}

	@Override protected void onStop () {
		if ( mGetUserTask != null && mGetUserTask.isInTransaction () ) mGetUserTask.cancelTransaction ();
		if ( mLogOutTask != null && mLogOutTask.isInTransaction () ) mLogOutTask.cancelTransaction ();
		if ( mStopCyclingTask != null && mStartCyclingTask.isInTransaction () ) mStartCyclingTask.cancelTransaction ();
		if ( mStopCyclingTask != null && mStopCyclingTask.isInTransaction () ) mStopCyclingTask.cancelTransaction ();
		if ( mUpdateCyclingTask != null && mUpdateCyclingTask.isInTransaction () ) mUpdateCyclingTask.cancelTransaction ();
		super.onStop ();
	}

	/** Fetches User ID from {@link LoginActivity} */
	private void getDataFromPreviousActivity () {
    	Intent intent = getIntent ();
    	mUserID = intent.getStringExtra ( Constants.USER_ID );
	}

	/**Initializes the Components used in this Activity.
	 * Components included are:
	 * <ul>
	 *     <li>{@link FragmentManager}</li>
	 *     <li>Realm DB Tasks ( {@link GetUserTask}, {@link LogOutUserTask}, {@link StartCyclingTask}, {@link StopCyclingTask}, {@link UpdateCyclingTask} )</li>
	 *     <li>Fragments ( {@link HomeFragment} )</li>
	 *     <li>{@link GPSLocation}</li>
	 *     <li>Instances of {@link LatLng}</li>
	 * </ul>*/
	private void initializeComponents () {
		//FragmentManager.
    	mFragmentManager = getSupportFragmentManager ();

		// Realm DB Tasks.
    	mGetUserTask = new GetUserTask ( MainActivity.this );
    	mLogOutTask = new LogOutUserTask ( MainActivity.this );
    	mStartCyclingTask = new StartCyclingTask ( MainActivity.this );
    	mStopCyclingTask = new StopCyclingTask ( MainActivity.this );
    	mUpdateCyclingTask = new UpdateCyclingTask ( MainActivity.this );
    	mGetAllTripsTask = new GetAllTripsTask ( MainActivity.this );

    	// Instance of Fragments.
    	mHomeFragment = new HomeFragment ();
    	mContributionFragment = new ContributionFragment ();
    	mHelpFragment = new HelpFragment ();
    	mNavigateFragment = new NavigateFragment ();
    	mTrackFragment = new TrackFragment ();

    	// Location Service Helper Class.
    	mLocation = new GPSLocation (
    			( LocationManager ) getSystemService ( Context.LOCATION_SERVICE ),
			    MainActivity.this,
			    MainActivity.this
	    );

    	// Location Coordinates.
    	mCurrentPosition = new LatLng ();
    	mPreviousPosition = new LatLng ( 0.0, 0.0 );
    }

    /**Sets the Toolbar onto ActionBar.
     * Also, sets the Title, Visibility of Home button. */
    private void initializeActionBar () {
	    mToolbar = findViewById ( R.id.activity_main_toolbar ); // Initialize Toolbar.
    	setSupportActionBar ( mToolbar ); // Set Toolbar to ActionBar.

	    ActionBar mActionBar = getSupportActionBar (); // Get instance of ActionBar.
	    if ( mActionBar != null ) {
	    	mActionBar.setDisplayHomeAsUpEnabled ( true ); // Display Home Button.
	    	mActionBar.setHomeButtonEnabled ( true ); // Enable Home Button.
	    	mActionBar.setTitle ( "" ); // Set Title for ActionBar.
	    }
    }

	/** Initializes the View Components of this Activity. */
	@SuppressWarnings ( "deprecation" ) private void initializeViews () {
    	mNVHome.setNavigationItemSelectedListener ( MainActivity.this );
    	customizeNavigationMenu ( mNVHome.getMenu () );
    	View header = mNVHome.getHeaderView ( 0 );
    	mTvUserName = header.findViewById ( R.id.activity_home_navigation_view_text_user_name );
	    /*ImageView mIvUserImage = header.findViewById ( R.id.activity_home_navigation_menu_user_image );*/
	    ActionBarDrawerToggle drawerToggle =
			    new ActionBarDrawerToggle (
					    MainActivity.this,
					    mDrawerLayout,
					    R.string.activity_main_drawer_open,
					    R.string.activity_main_drawer_close
			    );
	    mDrawerLayout.setDrawerListener ( drawerToggle );
	    drawerToggle.setDrawerIndicatorEnabled ( true );
	    drawerToggle.syncState ();
    }

    /** Customizes the View of Navigation Drawer Menu by setting ROBOTO LIGHT font to the Drawer Items. */
    private void customizeNavigationMenu ( @NonNull Menu menu ) {
	    Typeface tf1 = Typeface.createFromAsset ( getAssets (), "fonts/roboto-light.ttf" ); // Initialize the TypeFace by specifying the Font.
	    for ( int i = 0; i < menu.size (); ++i ) { /* Iterate over Navigation Menu. */
	    	MenuItem item = menu.getItem ( i ); // Get a particular menu item from Group.
		    SpannableString spannableString = new SpannableString ( item.getTitle () ); // Initialize Spannable String with MenuItem's Title.
		    spannableString.setSpan (
				    new CustomTypefaceSpan ( "", tf1 ),
				    0,
				    spannableString.length (),
				    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
		    ); // Set the Span for this SpannableString.
		    item.setTitle ( spannableString ); // Set the Title of MenuItem as SpannableString.
	    }
    }

    /** Uses the Realm DB Task {@link GetUserTask} to fetch the Current User. */
    private void fetchCurrentUser () {
    	ConstantMethods.showProgress ( MainActivity.this );
    	mGetUserTask.setValues ( mUserID );
    	mGetUserTask.execute ();
    }

	@Override public boolean onNavigationItemSelected ( @NonNull MenuItem item ) {
    	mDrawerLayout.closeDrawer ( Gravity.START );
    	switch ( item.getItemId () ) {
		    case R.id.activity_home_navigation_drawer_menu_item_home : { /* Navigation Menu Item : HOME */
			    showHomeFragment ();
		    } break;

		    case R.id.activity_home_navigation_drawer_menu_item_contribution : { /* Navigation Menu Item : CONTRIBUTION */
		    	showContributionFragment ();
		    } break;

		    case R.id.activity_home_navigation_drawer_menu_item_help : { /* Navigation Menu Item : HELP */
		    	showHelpFragment ();
		    } break;

		    case R.id.activity_home_navigation_drawer_menu_item_logout : { /* Navigation Menu Item : LOGOUT */
		    	logOut ();
		    } break;
	    } return false;
	}

	/**Replaces Existing Fragment with {@link HomeFragment}.
	 * Also sets Navigation Drawer's selected item to Home.*/
	private void showHomeFragment () {
		replaceFragment ( mHomeFragment );
		mNVHome.getMenu ().getItem ( 0 ).setChecked ( true );
	}

	/**Replaces Existing Fragment with ContributionFragment
	 * Also sets Navigation Drawer's selected item to Contribution.*/
	private void showContributionFragment () {
		replaceFragment ( mContributionFragment );
		mNVHome.getMenu ().getItem ( 1 ).setChecked ( true );
	}

	/**Replaces Existing Fragment with HelpFragment
	 * Also sets Navigation Drawer's selected item to Help.*/
	private void showHelpFragment () {
		replaceFragment ( mHelpFragment );
		mNVHome.getMenu ().getItem ( 2 ).setChecked ( true );
	}

	/**Runs the Realm DB Task {@link LogOutUserTask}
	 * Also sets Navigation Drawer's selected item to LogOut.*/
	private void logOut () {
		mNVHome.getMenu ().getItem ( 3 ).setChecked ( true );
		ConstantMethods.showProgress ( MainActivity.this );
		mLogOutTask.setValues ( mUserID );
		mLogOutTask.execute ();
	}

	/**Replaces the given Fragment as parameter with the fragment Displayed.
	 * @param fragment Instance of Fragment to replace with.*/
	private void replaceFragment ( Fragment fragment ) {
		String fragmentTag = fragment.getClass ().getCanonicalName (); // Set Unique Name for the fragment, which will be used as tag.
		boolean isPopped = mFragmentManager.popBackStackImmediate ( fragmentTag, 0 ); // Trying to return to the previous state of B if it exists.
		if ( ! isPopped && mFragmentManager.findFragmentByTag ( fragmentTag ) == null ) { // fragment is not in the back-stack, so create it.
			FragmentTransaction ft = mFragmentManager.beginTransaction ();
			ft.replace ( R.id.activity_main_fragment_content, fragment, fragmentTag );
			ft.addToBackStack ( fragmentTag );
			ft.commit ();
		}
	}

	@Override public void onHomeResumed () {
		mToolbar.setBackgroundColor ( getResources ().getColor ( R.color.color_activity_main_toolbar_background ) );
		mNVHome.getMenu ().getItem ( 0 ).setChecked ( true );
	}

	@Override public void onContributionResumed () {
		mToolbar.setBackgroundColor ( Color.parseColor ( "#2c8248" ) );
		mNVHome.getMenu ().getItem ( 1 ).setChecked ( true );
	}

	@Override public void onFetchAllTrips () { callFetchAllTripsTask (); }

	private void callFetchAllTripsTask () {
		ConstantMethods.showProgress ( MainActivity.this );
		mGetAllTripsTask.setData ( mUserID );
		mGetAllTripsTask.execute ();
	}

	@Override public void onHelpResumed () {
		mNVHome.getMenu ().getItem ( 2 ).setChecked ( true );
	}

	@Override public void startCycling () {
    	if ( mCurrentPosition.getLatitude () == 9999.9999 || mCurrentPosition.getLongitude () == 9999.9999 )
    		ConstantMethods.showToastMessage ( MainActivity.this, "Current Location Not Acquired" );
    	else callStartCyclingTask ();
	}

	/**Starts the execution of Realm DB Task : {@link StartCyclingTask}
	 * Requirement to Start Cycling Task :
	 * <ul>
	 *     <li>User ID</li>
	 *     <li>Current Location</li>
	 *     <li>Current Time of System</li>
	 * </ul>*/
	private void callStartCyclingTask () {
		ConstantMethods.showProgress ( MainActivity.this );
		mStartCyclingTask.setValues ( mUserID, mCurrentPosition, System.currentTimeMillis () );
		mStartCyclingTask.execute ();
	}

	@Override public void stopCycling () { callStopCyclingTask (); }

	/**Starts the execution of Realm DB Task : {@link StopCyclingTask}
	 * Requirement to Stop Cycling Task :
	 * <ul><li>User ID</li></ul>*/
	private void callStopCyclingTask () {
		ConstantMethods.showProgress ( MainActivity.this );
		mStopCyclingTask.setValues ( mUserID );
		mStopCyclingTask.execute ();
	}

	@Override public void onUserLoggedOut () {
		ConstantMethods.removeProgress ();
		startLoginActivity ();
	}

	@Override public void errorOnLoggingUserOut ( final String error ) {
    	runOnUiThread (
			    new Runnable () {
				    @Override public void run () {
					    ConstantMethods.removeProgress ();
					    ConstantMethods.showToastMessage ( MainActivity.this, error );
				    }
			    }
	    );
	}

	/**Starts {@link LoginActivity} and finishes this Activity.*/
	private void startLoginActivity () {
		Intent intent = new Intent ( MainActivity.this, LoginActivity.class );
    	startActivity ( intent );
    	finish ();
	}

	@Override public void onUserAcquired ( User user ) {
		android.util.Log.e ( "Method", "onUserAcquired ()" );
		ConstantMethods.removeProgress ();
		ConstantMethods.showToastMessage ( MainActivity.this, "User Acquired" );
		mUser = user;
		mTvUserName.setText ( user.getUserName () );
		RealmList<Trip> trips = mUser.getTrip ();
		if ( trips.size () > 0 ) {
			Trip trip = trips.last ();
			if ( trip != null ) {
				if ( trip.isTripStarted () ) isTripStarted = true;
			}
		}
		if ( isTripStarted ) resumeCycling ();
	}

	private void resumeCycling () {
		Trip trip = mUser.getTrip ().last ();
		if ( trip == null ) ConstantMethods.showToastMessage ( MainActivity.this, "Trip Data Corrupted" );
		else {
			mDistance = trip.getDistanceSaved ();
			LatLng previousLocation = trip.getTripLastKnownPosition ();
			if ( previousLocation != null ) {
				mPreviousPosition.setLatitude ( previousLocation.getLatitude () );
				mPreviousPosition.setLongitude ( previousLocation.getLongitude () );
			}
			mHomeFragment.startUpdateOfTimeInterval ();
			mHomeFragment.updateDistanceOnDistanceCardView ( mDistance );
		}
	}

	/**Get the Instance of Model Class {@link User} containing the Latest Information of User.
	 * @return Instance of {@link User}.*/
	public User getUser () { return mUser; }

	@Override public void errorOnAcquiringUser ( String error ) {
    	ConstantMethods.removeProgress ();
    	ConstantMethods.showToastMessage ( MainActivity.this, error );
	}

	@Override protected void onDestroy () {
		super.onDestroy ();
		mHomeFragment.stopUpdateOfTimeInterval ();
		LatLng latLng = new LatLng ( mCurrentPosition.getLatitude (), mCurrentPosition.getLongitude () );
		mUpdateCyclingTask.setValues ( mUserID, latLng, mDistance );
		mUpdateCyclingTask.execute ();
	}

	@Override public void onChangedLocation ( Location location ) {
		// Set the Latitude and Longitude of Current Position.
		mCurrentPosition.setLatitude ( location.getLatitude () );
		mCurrentPosition.setLongitude ( location.getLongitude () );
		android.util.Log.e ( "onChangedLocation", mCurrentPosition.toString () );
		if ( isTripStarted ) handleLocationChange ( location );
	}

	private void handleLocationChange ( Location location ) {
		// Check whether Previous Location has been assigned or not.
		if ( mPreviousPosition.getLatitude () == 0.0 || mPreviousPosition.getLongitude () == 0.0 )
			mDistance += 0.0; // Add the distance as 0m.
		else
			mDistance += ConstantMethods.distance ( mPreviousPosition, mCurrentPosition ); // Add the calculated distance.

		// Save the Current Position's Latitude and Longitude values in mPreviousPosition.
		mPreviousPosition.setLatitude ( location.getLatitude () );
		mPreviousPosition.setLongitude ( location.getLongitude () );

		mHomeFragment.updateSpeedOnSpeedometerView ( location.getSpeed () );
		mHomeFragment.updateDistanceOnDistanceCardView ( mDistance );
	}

	@Override public void onCyclingStartSuccess ( String tripID ) {
    	isTripStarted = true;
    	mHomeFragment.startUpdateOfTimeInterval ();
    	mGetUserTask.setValues ( mUserID );
    	mGetUserTask.execute ();
	}

	@Override public void onCyclingStartError ( final String error ) {
    	runOnUiThread (
			    new Runnable () {
				    @Override public void run () {
					    ConstantMethods.removeProgress ();
					    ConstantMethods.showToastMessage ( MainActivity.this, error );
					    isTripStarted = false;
				    }
			    }
	    );
	}

	@Override public void onCyclingStopped () {
    	ConstantMethods.removeProgress ();
    	mHomeFragment.stopUpdateOfTimeInterval ();
    	isTripStarted = false;
	}

	@Override public void errorOnStoppingCycling ( final String error ) {
    	runOnUiThread (
			    new Runnable () {
				    @Override public void run () {
					    ConstantMethods.removeProgress ();
					    ConstantMethods.showToastMessage ( MainActivity.this, error );
				    }
			    }
	    );
	}

	@Override public void onCyclingTripUpdated () { android.util.Log.e ( "Method", "onCyclingTripUpdated ()" ); }

	@Override public void errorOnUpdatingCyclingTrip ( final String error ) {
    	runOnUiThread (
			    new Runnable () {
				    @Override public void run () {
					    ConstantMethods.removeProgress ();
					    ConstantMethods.showToastMessage ( MainActivity.this, error );
				    }
			    }
	    );
	}

	private FragmentNames getInstanceOfFragment () {
		Fragment fragment = getSupportFragmentManager ().findFragmentById ( R.id.activity_main_fragment_content );
		if ( fragment instanceof HomeFragment ) return FragmentNames.HOME;
		else if ( fragment instanceof ContributionFragment ) return FragmentNames.CONTRIBUTION;
		else if ( fragment instanceof HelpFragment ) return FragmentNames.HELP;
		else if ( fragment instanceof NavigateFragment ) return FragmentNames.NAVIGATE;
		else if ( fragment instanceof TrackFragment ) return FragmentNames.TRACK;
		else return FragmentNames.HOME;
	}

	@Override public void onAllTripsFetched ( RealmList<Trip> trips ) {
		ConstantMethods.removeProgress ();
		if ( trips == null ) setTotalDistance ( 0 );
		else calculateTotalDistance ( trips );
	}

	private void setTotalDistance ( double distance ) { mTotalDistance = distance; }
	public double getTotalDistance () { return mTotalDistance; }
	private void calculateTotalDistance ( RealmList<Trip> trips ) {
		double totalDistance = 0.0;
		for ( Trip trip : trips ) totalDistance += trip.getDistanceSaved ();
		setTotalDistance ( totalDistance );
	}

	@Override public void errorOnFetchingAllTrips ( String error ) {
		ConstantMethods.removeProgress ();
		ConstantMethods.showToastMessage ( MainActivity.this, error );
	}

	private enum FragmentNames { HOME, CONTRIBUTION, HELP, NAVIGATE, TRACK }
}