package io.recro.ecodrive.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import io.recro.ecodrive.R;
import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.realmDBTasks.FindUserForPasswordForgottenTask;
import io.recro.ecodrive.utilities.ConstantMethods;

public class ForgotPasswordActivity
		extends AppCompatActivity
		implements View.OnFocusChangeListener,
		           FindUserForPasswordForgottenTask.FindUserForPasswordForgottenListener {
	// Views
	@BindView ( R.id.activity_forgot_password_text_input_layout_bike_id ) TextInputLayout mTILBikeID;
	@BindView ( R.id.activity_forgot_password_text_input_edit_text_bike_id ) TextInputEditText mTIETBikeID;
	@BindView ( R.id.activity_forgot_password_text_input_layout_mobile_number) TextInputLayout mTILMobileNumber;
	@BindView ( R.id.activity_forgot_password_text_input_edit_text_mobile_number ) TextInputEditText mTIETMobileNumber;

	// String Resources
	@BindString ( R.string.activity_forgot_password_text_bike_id_caps ) String sBikeIDCaps;
	@BindString ( R.string.activity_forgot_password_text_bike_id_no_caps ) String sBikeIDNoCaps;
	@BindString ( R.string.activity_forgot_password_text_mobile_number_caps ) String sMobileNumberCaps;
	@BindString ( R.string.activity_forgot_password_text_mobile_number_no_caps ) String sMobileNumberNoCaps;

	// Realm DB Tasks.
	private FindUserForPasswordForgottenTask mTask;

	@Override protected void onCreate ( Bundle savedInstanceState ) {
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_forgot_password );
		ButterKnife.bind ( ForgotPasswordActivity.this );
		initializeComponents ();
		initializeViews ();
	}

	@Override protected void onStop () {
		super.onStop ();
		if ( mTask != null && mTask.isInTransaction () ) mTask.cancelTransaction ();
	}

	/**<p>Initializes the Components for this activity.</p>
	 * <p>Components include :
	 * <ul>
	 *     <li>Realm DB Task ( {@link FindUserForPasswordForgottenTask} )</li>
	 * </ul>
	 * </p>*/
	private void initializeComponents () {
		mTask = new FindUserForPasswordForgottenTask ( ForgotPasswordActivity.this );
	}

	/**<p>Initializes the Views.</p>
	 * <p>Initiation of Views includes :
	 * <ul>
	 *     <li>Setting Hint for {@link TextInputLayout}s.</li>
	 *     <li>Setting Focus Change Listener for {@link TextInputEditText}s.</li>
	 *     <li>Setting On-Editor Action Listener for {@code mTIETMobileNumber}.</li>
	 * </ul>
	 * </p>*/
	private void initializeViews () {
		// Set Hint to TextInputLayout.
		mTILBikeID.setHint ( sBikeIDNoCaps );
		mTILMobileNumber.setHint ( sMobileNumberNoCaps );

		// Manage the TextInputEditTexts Focus Animations.
		mTIETBikeID.setOnFocusChangeListener ( ForgotPasswordActivity.this );
		mTIETMobileNumber.setOnFocusChangeListener ( ForgotPasswordActivity.this );

		// Implement onEditorActionListener to Password Field so that when user clicks on Enter, Login Service shall hit.
		final View view = this.getCurrentFocus();
		mTIETMobileNumber.setOnEditorActionListener (
				new TextView.OnEditorActionListener () {
					@Override public boolean onEditorAction ( TextView textView, int id, KeyEvent keyEvent ) {
						if ( id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL ) {
							if ( view != null ) {
								InputMethodManager imm = (InputMethodManager) getSystemService ( Context.INPUT_METHOD_SERVICE );
								if ( imm != null ) imm.hideSoftInputFromWindow ( view.getWindowToken(), 0 );
							}
							resetPassword ();
							return true;
						} return false;
					}
				}
		);
	}

	@Override public void onFocusChange ( View view, boolean hasFocus ) {
		switch ( view.getId () ) {
			case R.id.activity_forgot_password_text_input_edit_text_bike_id : {
				if ( hasFocus ) mTILBikeID.setHint ( sBikeIDCaps );
				else if ( mTIETBikeID.getText ().toString ().isEmpty () ) mTILMobileNumber.setHint ( sBikeIDNoCaps );
			} break;
			case R.id.activity_forgot_password_text_input_edit_text_mobile_number : {
				if ( hasFocus ) mTILMobileNumber.setHint ( sMobileNumberCaps );
				else if ( mTIETMobileNumber.getText ().toString ().isEmpty () ) mTILMobileNumber.setHint ( sMobileNumberNoCaps );
			} break;
		}
	}

	@OnClick ( R.id.activity_forgot_password_relative_layout_reset ) void resetPassword () {
		if ( validate () ) {
			String bikeID = mTIETBikeID.getText ().toString ();
			String mobileNumber = mTIETMobileNumber.getText ().toString ();
			ConstantMethods.showProgress ( ForgotPasswordActivity.this );
			mTask.setData ( bikeID, mobileNumber );
			mTask.execute ();
		}
	}

	private boolean validate () {
		boolean isValid = false;
		if ( mTIETBikeID.getText ().toString ().isEmpty () ) {
			mTIETBikeID.setError ( "Please Enter Bike ID" );
			mTIETBikeID.requestFocus ();
		} else if ( mTIETMobileNumber.getText ().toString ().length () == 0 ) {
			mTIETMobileNumber.setError ( "Please Enter Mobile Number" );
			mTIETMobileNumber.requestFocus ();
		} else if ( mTIETMobileNumber.getText ().toString ().length () != 10 ) {
			mTIETMobileNumber.setError ( "Please Enter Valid Mobile Number" );
			mTIETMobileNumber.requestFocus ();
		} else isValid = true;
		return isValid;
	}

	@Override public void onUserFound ( String userID ) {
		ConstantMethods.removeProgress ();
		startChangePasswordActivity ( userID );
	}

	@Override public void errorOnFindingUser ( String error ) {
		ConstantMethods.removeProgress ();
		ConstantMethods.showToastMessage ( ForgotPasswordActivity.this, error );
	}

	private void startChangePasswordActivity ( String userID ) {
		Intent intent = new Intent ( ForgotPasswordActivity.this, ChangePasswordActivity.class );
		Bundle bundle = new Bundle ();
		bundle.putString ( FieldConstants.USER_ID, userID );
		intent.putExtras ( bundle );
		startActivity ( intent );
		finish ();
	}
}