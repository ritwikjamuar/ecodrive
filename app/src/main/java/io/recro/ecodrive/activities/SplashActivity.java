package io.recro.ecodrive.activities;

import android.Manifest;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.location.LocationManager;

import android.os.Build;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;

import butterknife.BindString;
import butterknife.ButterKnife;

import io.recro.ecodrive.R;

import io.recro.ecodrive.realmDBTasks.FindLoggedInUserTask;
import io.recro.ecodrive.utilities.ConstantMethods;

import io.recro.ecodrive.utilities.infoDialog.ButtonType;
import io.recro.ecodrive.utilities.infoDialog.InfoAction;
import io.recro.ecodrive.utilities.infoDialog.InfoDialog;
import io.recro.ecodrive.utilities.infoDialog.OnInfoAction;

/**The very first screen of this Application.
 * This screen will show Application login_logo for 3 Seconds.
 * After 3 Seconds, it will redirect Application to {@link LoginActivity}.*/
public class SplashActivity
		extends AppCompatActivity
		implements OnInfoAction,
		           FindLoggedInUserTask.FindLoggedInListener {
	// Components.
	private LocationManager mLocationManager;

	// Constants.
	private final int MY_PERMISSIONS_REQUEST_PERMISSIONS = 101, LOCATION_ON_ACTIVITY_RESULT = 102;
	private final String ENABLE_LOCATION = "enableLocation", LOCATION_DIALOG = "locationDialog";

	// Resource Strings.
	@BindString ( R.string.location_request ) String sLocationRequest;
	@BindString ( R.string.location_error ) String sLocationError;

	// Variables
	private String mUserID;

	// Realm DB Tasks.
	private FindLoggedInUserTask mTask;

	// Handler
	private Handler mHandler;

    @Override protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_splash_screen );
        initializeComponents ();
	    checkLocationPermission ();
    }

	/** Initializes the component. */
	private void initializeComponents () {
		ButterKnife.bind ( SplashActivity.this );
		mLocationManager = (LocationManager) getSystemService ( Context.LOCATION_SERVICE );
		mTask = new FindLoggedInUserTask ( SplashActivity.this );
		mHandler = new Handler ();
	}

	@Override protected void onStop () {
		super.onStop ();
		if ( mTask != null && mTask.isInTransaction () ) mTask.cancelTransaction ();
	}

	@Override protected void onResume () {
		super.onResume ();
	}

	/** Checks whether the Location service is permitted for use. */
	private void checkLocationPermission () {
		if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
			boolean noFineLocation = ActivityCompat.checkSelfPermission ( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED;
			boolean noCoarseLocation = ActivityCompat.checkSelfPermission ( this, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED;
			if ( noFineLocation && noCoarseLocation ) requestLocationPermission ();
			else checkIsLocationServiceEnabled ();
		} else checkIsLocationServiceEnabled ();
	}

	/** Requests the Permission for Location Service. */
	private void requestLocationPermission () {
		ActivityCompat.requestPermissions (
				SplashActivity.this,
				new String[] {
						Manifest.permission.ACCESS_COARSE_LOCATION,
						Manifest.permission.ACCESS_FINE_LOCATION,
				},
				MY_PERMISSIONS_REQUEST_PERMISSIONS );
	}

	@Override public void onRequestPermissionsResult ( int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults ) {
		super.onRequestPermissionsResult ( requestCode, permissions, grantResults );
		switch ( requestCode ) {
			case MY_PERMISSIONS_REQUEST_PERMISSIONS : {
				if ( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
					mLocationManager = ( LocationManager ) getSystemService ( Context.LOCATION_SERVICE );
					checkIsLocationServiceEnabled ();
				}
			} break;
		}
	}

	/** Check whether LocationID service is enabled in Device or not. */
	private void checkIsLocationServiceEnabled () {
		boolean networkProvider = mLocationManager.isProviderEnabled ( LocationManager.NETWORK_PROVIDER );
		boolean gpsProvider = mLocationManager.isProviderEnabled ( LocationManager.GPS_PROVIDER );
		if ( networkProvider && gpsProvider ) continueApp ();
		else if ( ! InfoDialog.isDialogShown () )
			showInfoDialog ();
	}

	/** Shows the Info Dialog of Enabling the LocationID Service. */
	private void showInfoDialog () {
		InfoDialog.showDialog (
				SplashActivity.this,
				null,
				sLocationRequest,
				ButtonType.CUSTOM,
				"Enable",
				"Cancel",
				SplashActivity.this,
				ENABLE_LOCATION
		);
	}

	@Override public void onInfoActionClick ( InfoAction infoAction, String callBackReference ) {
		if ( LOCATION_DIALOG.equals ( callBackReference ) || ENABLE_LOCATION.equals ( callBackReference ) ) {
			if ( InfoAction.POSITIVE.equals ( infoAction ) ) {
				InfoDialog.dismiss ();
				startLocationSettingsActivity ();
			} else closeAppWithToast ();
		}
	}

	/** Closes the app with a Message. */
	private void closeAppWithToast () {
		ConstantMethods.showToastMessage ( SplashActivity.this, "The app won't work without LocationID Service" );
		SplashActivity.this.finish ();
	}

	/** Continues the Application */
	private void continueApp () {
		mTask.execute ();
	}

	/** Starts Location Settings of the Device in order to prompt user to enable his/her Location. */
	private void startLocationSettingsActivity () {
		try {
			startActivityForResult (
					new Intent ( android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS ),
					LOCATION_ON_ACTIVITY_RESULT
			);
		} catch ( ActivityNotFoundException e ) { ConstantMethods.showToastMessage ( SplashActivity.this, sLocationError ); }
	}

	@Override protected void onActivityResult ( int requestCode, int resultCode, Intent data ) {
		super.onActivityResult ( requestCode, resultCode, data );
		if ( requestCode == LOCATION_ON_ACTIVITY_RESULT ) {
			boolean networkProvider = mLocationManager.isProviderEnabled ( LocationManager.NETWORK_PROVIDER );
			boolean gpsProvider = mLocationManager.isProviderEnabled ( LocationManager.GPS_PROVIDER );
			if ( networkProvider && gpsProvider ) continueApp ();
			else closeAppWithToast ();
		} else closeAppWithToast ();
	}

	@Override public void onLoggedInUserFound ( String userID ) {
    	mUserID = userID;
    	mHandler.postDelayed ( mainRunnable, 3000 );
	}

	@Override public void onLoggedInUserNotFound () {
		mHandler.postDelayed ( loginRunnable, 3000 );
	}

	@Override public void errorOnFindingLoggedInUser ( String error ) {
    	ConstantMethods.showToastMessage ( SplashActivity.this, error );
	}

	/** Runnable to Start {@link LoginActivity}. */
	private Runnable loginRunnable = new Runnable() {
		@Override public void run () {
			startLoginActivity ();
		}
	};

	/** Runnable to Start {@link MainActivity}.*/
    private Runnable mainRunnable = new Runnable () {
	    @Override public void run () {
		    startMainActivity ();
	    }
    };

	/** Starts {@link LoginActivity} */
	private void startLoginActivity () {
		startActivity ( new Intent ( SplashActivity.this, LoginActivity.class ) );
		finish ();
	}

	/** Starts  {@link MainActivity} */
	private void startMainActivity () {
		Intent intent = new Intent ( SplashActivity.this, MainActivity.class );
		intent.putExtra ( "userID", mUserID );
		startActivity ( intent );
		finish ();
	}
}