package io.recro.ecodrive.fragments;

import android.content.Context;

import android.net.Uri;

import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.recro.ecodrive.R;

/**<p>Shows Direction between two Locations given by User.</p>
 * <p>Location will be a text denoting address in textual format.</p>
 * <p>Components of this Fragment
 * <ul>
 *     <li>Two Text Fields for Entering Address.</li>
 *     <li>Map View to show Direction between two points.</li>
 * </ul></p>
 * <p>A Special Case follows in this Fragment, that the Start Location will be by-default the Current Location.</p>
 * <p>A simple {@link Fragment} subclass.</p>
 * <p>Activities that contain this fragment must implement the
 * {@link TrackFragment.TrackListener} interface to handle interaction events.</p>*/
public class TrackFragment extends Fragment {
	private TrackListener mListener;

	public TrackFragment () {}

	@Override public void onCreate ( @Nullable Bundle savedInstanceState ) {
		super.onCreate ( savedInstanceState );
	}

	@Override public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		// Inflate the layout for this fragment
		return inflater.inflate ( R.layout.fragment_track, container, false );
	}

	@Override public void onAttach ( Context context ) {
		super.onAttach ( context );
		if ( context instanceof TrackListener ) mListener = ( TrackListener ) context;
		else throw new RuntimeException ( context.toString () + " must implement OnFragmentInteractionListener" );
	}

	@Override public void onDetach () {
		super.onDetach ();
		mListener = null;
	}

	/**Interface providing methods to keep interaction with the Activities using {@link HomeFragment}.*/
	public interface TrackListener {
		void onFragmentInteraction ( Uri uri );
	}
}