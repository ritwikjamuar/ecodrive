package io.recro.ecodrive.fragments;

import android.content.Context;

import android.os.Bundle;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import io.realm.RealmList;
import io.recro.ecodrive.R;

import io.recro.ecodrive.activities.MainActivity;

import io.recro.ecodrive.models.Trip;
import io.recro.ecodrive.models.User;

import io.recro.ecodrive.utilities.ConstantMethods;
import io.recro.ecodrive.utilities.customViews.FloatingActionMenu;
import io.recro.ecodrive.utilities.customViews.speedometer.SpeedometerView;

/**<p>Shows Cycling Components based Interactions.</p>
 * <p>
 *     Following Components are shown:
 *     <ul>
 *         <li>Speedometer.</li>
 *         <li>Distance Counter.</li>
 *         <li>Time Counter.</li>
 *         <li>Button to Toggle between {@code Start Cycling} and {@code Stop Cycling}.</li>
 *     </ul>
 * </p>
 * <p>From this Fragment, user controls Cycling Operations and shown Respective Components.</p>
 * <p>A simple {@link Fragment} subclass.</p>
 * <p>Activities that contain this fragment must implement the {@link HomeFragment.HomeListener}
 * interface to handle interaction events.</p>*/
public class HomeFragment extends Fragment {
	// Views
	@BindView ( R.id.fragment_home_floating_action_menu ) FloatingActionMenu mFloatingActionMenu;
	@BindView ( R.id.fragment_home_floating_action_button_invoke ) FloatingActionButton mFABInvoke;
	@BindView ( R.id.fragment_home_floating_action_button_track ) FloatingActionButton mFABTrack;
	@BindView ( R.id.fragment_home_floating_action_button_navigate ) FloatingActionButton mFABNavigate;
	@BindView ( R.id.fragment_home_floating_action_button_fingerprint ) FloatingActionButton mFABFingerprint;
	@BindView ( R.id.fragment_home_button_start_cycling ) ImageView mIvStartCycling;
	@BindView ( R.id.fragment_home_text_distance ) TextView mTvDistance;
	@BindView ( R.id.fragment_home_text_duration ) TextView mTvDuration;
	@BindView ( R.id.fragment_home_speedometer ) SpeedometerView mSvSpeedometer;

	// Listeners.
	private HomeListener mListener;

	// Models.
	private User mUser;

	public HomeFragment () {}

	@Override public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		// Inflate the layout for this fragment.
		View view = inflater.inflate ( R.layout.fragment_home, container, false );
		ButterKnife.bind ( HomeFragment.this, view ); // Bind the view with ButterKnife.
		return view;
	}

	@Override public void onResume () {
		super.onResume ();
		if ( mListener != null ) mListener.onHomeResumed ();
	}

	@Override public void onDestroy () {
		timerThread.interrupt ();
		super.onDestroy ();
	}

	/** Get the Instance of {@link User} from {@link MainActivity}.*/
	private void getUser () {
		MainActivity activity = (MainActivity) getActivity ();
		mUser = activity.getUser ();
	}

	@Override public void onAttach ( Context context ) {
		super.onAttach ( context );
		if ( context instanceof HomeListener ) { mListener = ( HomeListener ) context; }
		else { throw new RuntimeException ( context.toString () + " must implement OnFragmentInteractionListener" ); }
	}

	@Override public void onDetach () {
		super.onDetach ();
		mListener = null;
	}

	@OnClick ( R.id.fragment_home_button_start_cycling ) void startCycling () {
		getUser ();
		if ( mUser != null ) {
			RealmList<Trip> trips = mUser.getTrip ();
			if ( trips == null ) ConstantMethods.showToastMessage ( getActivity (), "Trip Data Corrupted" );
			else {
				if ( trips.size () > 0 ) {
					Trip trip = trips.last ();
					if ( trip == null ) ConstantMethods.showToastMessage ( getActivity (), "Trip Data Corrupted" );
					else {
						if ( trip.isTripStarted () ) mListener.stopCycling ();
						else mListener.startCycling ();
					}
				} else mListener.startCycling ();
			}
		} else ConstantMethods.showToastMessage ( getActivity (), "User not Found" );
	}

	/**Starts the {@code timerThread} for constantly updating the timer on this Fragment.
	 * @param speed Value of Speed to be updated in {@link SpeedometerView}.*/
	public void updateSpeedOnSpeedometerView ( float speed ) {
		float speedInKmH = ConstantMethods.convertSpeed ( speed );
		mSvSpeedometer.setCurrentSpeed ( speedInKmH );
	}

	/**Updates the Distance Card View.
	 * @param distance Distance Travelled in km.*/
	public void updateDistanceOnDistanceCardView ( double distance ) {
		double distanceOnKm = distance / 1000;
		DecimalFormat format = new DecimalFormat ( "#.0" );
		String distanceText = format.format ( distanceOnKm );
		if ( distanceText.length () == 2 ) distanceText = "0" + distanceText;
		mTvDistance.setText ( distanceText );
	}

	/**Starts the {@code timerThread} for constantly updating the timer on this Fragment.*/
	public void startUpdateOfTimeInterval () { timerThread.start (); }

	/**Stops the {@code timerThread} on this Fragment.*/
	public void stopUpdateOfTimeInterval () { timerThread.interrupt (); }

	/**A {@link Thread} that updates the timer on this Fragment.*/
	Thread timerThread = new Thread () {
		@Override public void run () {
			try {
				while ( ! isInterrupted () ) {
					android.util.Log.e ( "isInterrupted", String.valueOf ( isInterrupted () ) );
					Thread.sleep (1000 );
					MainActivity activity = ( MainActivity ) getActivity ();
					if ( activity != null ) {
						activity.runOnUiThread (
								new Runnable() {
									@Override public void run () {
										getUser ();
										String duration = "00:00:00";
										android.util.Log.e ( "User", "" + mUser.toString () );
										Trip trip = mUser.getTrip ().last ();
										if ( trip != null ) duration = getTimeElapsed ( trip );
										mTvDuration.setText ( duration );
									}
								}
						);
					}
				}
			} catch ( InterruptedException e ) { e.printStackTrace (); }
		}
	};

	/**<p>Evaluates the Time Elapsed in the Trip.</p>
	 * <p>Compares the value of {@code startTime} in {@link User} model
	 *    with Current Time of System ({@code System.currentTimeMillis()}
	 *    to get the Time Difference.</p>
	 * @return String representing Elapsed Time in the format : {@code hh:mm:ss}.*/
	private String getTimeElapsed ( Trip trip ) {
		String elapsedTime;
		long currentTime = System.currentTimeMillis ();
		long startTime = trip.getStartTime ();
		if ( startTime == 0 ) elapsedTime = "NA";
		else {
			long diff = currentTime - startTime;
			long diffSec = diff / 1000 % 60;
			long diffMin = diff / ( 60 * 1000 ) % 60;
			long diffHr = diff / ( 60 * 60 * 1000 );

			String hour;
			if ( diffHr < 10 ) hour = "0" + diffHr;
			else hour = String.valueOf ( diffHr );

			String minute;
			if ( diffMin < 10 ) minute = "0" + diffMin;
			else minute = String.valueOf ( diffMin );

			String second;
			if ( diffSec < 10 ) second = "0" + diffSec;
			else second = String.valueOf ( diffSec );

			elapsedTime = hour + ":" + minute + ":" + second;
		}
		return elapsedTime;
	}

	/**Interface providing methods to keep interaction with the Activities using {@link HomeFragment}.*/
	public interface HomeListener {
		/**<p>{@link HomeFragment}'s Listener Method indicating that {@link HomeFragment} has been resumed.</p>*/
		void onHomeResumed ();

		/**<p>{@link HomeFragment}'s Listener Method indicating to Start Cycling pressed by user.</p>
		 * <p>Invokes when Start Cycling has been clicked by the user.</p>*/
		void startCycling ();

		/**<p>{@link HomeFragment}'s Listener Method indicating to Stop Cycling pressed by user.</p>
		 * <p>Invokes when Stop Cycling has been clicked by the user.</p>*/
		void stopCycling ();
	}
}