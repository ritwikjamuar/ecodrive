package io.recro.ecodrive.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.recro.ecodrive.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HelpFragment.HelpListener} interface to handle interaction events.
 */
public class HelpFragment extends Fragment {
	private HelpListener mListener;

	public HelpFragment () {}

	@Override public void onCreate ( Bundle savedInstanceState ) {
		super.onCreate ( savedInstanceState );
	}

	@Override public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		// Inflate the layout for this fragment
		return inflater.inflate ( R.layout.fragment_help, container, false );
	}

	@Override public void onResume () {
		super.onResume ();
		if ( mListener != null ) mListener.onHelpResumed ();
	}

	@Override public void onAttach ( Context context ) {
		super.onAttach ( context );
		if ( context instanceof HelpListener ) mListener = ( HelpListener ) context;
		else throw new RuntimeException ( context.toString () + " must implement OnFragmentInteractionListener" );
	}

	@Override public void onDetach () {
		super.onDetach ();
		mListener = null;
	}

	/**Interface providing methods to keep interaction with the Activities using {@link HelpFragment}.*/
	public interface HelpListener {
		void onHelpResumed ();
	}
}