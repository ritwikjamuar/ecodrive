package io.recro.ecodrive.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.recro.ecodrive.R;
import io.recro.ecodrive.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContributionFragment.ContributionListener} interface
 * to handle interaction events.
 */
public class ContributionFragment extends Fragment {
	// Views
	@BindView ( R.id.fragment_contribution_text_view_carbon_footprint_wrt_cars ) TextView mTvCarbonFootprintWRTCars;
	@BindView ( R.id.fragment_contribution_text_view_carbon_footprint_wrt_bikes ) TextView mTvCarbonFootprintWRTBikes;

	private ContributionListener mListener;
	private double dTotalDistance;

	public ContributionFragment () {}

	@Override public void onCreate ( Bundle savedInstanceState ) {
		super.onCreate ( savedInstanceState );
		android.util.Log.e ( "Method", "onCreate ()" );
	}

	@Override public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		// Inflate the layout for this fragment
		android.util.Log.e ( "Method", "onCreateView ()" );
		View view = inflater.inflate ( R.layout.fragment_contribution, container, false );
		ButterKnife.bind ( ContributionFragment.this, view );
		return view;
	}

	@Override public void onResume () {
		super.onResume ();
		android.util.Log.e ( "Method", "onResume ()" );
		getTotalDistance ();
		setCarbonFootprintValuesToTextViews ();
		if ( mListener != null ) mListener.onContributionResumed ();
	}

	private void getTotalDistance () {
		MainActivity activity = ( MainActivity ) getActivity ();
		if ( activity == null ) dTotalDistance = 0.0;
		else dTotalDistance = activity.getTotalDistance ();
	}

	private void setCarbonFootprintValuesToTextViews () {
		mTvCarbonFootprintWRTCars.setText ( formatDouble ( carbonFootingSavedWRTCars () ) );
		mTvCarbonFootprintWRTBikes.setText ( formatDouble ( carbonFootingSavedWRTBikes () ) );
	}

	@Override public void onAttach ( Context context ) {
		super.onAttach ( context );
		if ( context instanceof ContributionListener ) mListener = ( ContributionListener ) context;
		else throw new RuntimeException ( context.toString () + " must implement OnFragmentInteractionListener" );
	}

	@Override public void onDetach () {
		super.onDetach ();
		mListener = null;
	}

	private double carbonFootingSavedWRTCars () {
		double c = dTotalDistance * 271;
		double b = dTotalDistance * 21;
		return c - b;
	}

	private double carbonFootingSavedWRTBikes () {
		double m = dTotalDistance * 100;
		double b = dTotalDistance * 21;
		return m - b;
	}

	private String formatDouble ( double value ) {
		DecimalFormat format = new DecimalFormat ( "#.0" );
		String distanceText = format.format ( value );
		if ( distanceText.length () == 2 ) distanceText = "0" + distanceText;
		return distanceText;
	}

	/**Interface providing methods to keep interaction with the Activities using {@link HomeFragment}.*/
	public interface ContributionListener {
		void onContributionResumed ();
		void onFetchAllTrips ();
	}
}