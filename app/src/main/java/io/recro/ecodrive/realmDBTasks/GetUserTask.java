package io.recro.ecodrive.realmDBTasks;

import io.realm.Realm;
import io.realm.RealmResults;
import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.models.User;

public class GetUserTask {
	private GetUserListener mListener;
	private Realm mRealm;
	private String mUserID;

	public GetUserTask ( GetUserListener mListener ) {
		this.mListener = mListener;
		onPreExecute ();
	}

	private void onPreExecute () { mRealm = Realm.getDefaultInstance (); }

	public void setValues ( String mUserID ) { this.mUserID = mUserID; }

	public void execute () {
		RealmResults<User> results =
				mRealm
						.where ( User.class )
						.equalTo ( FieldConstants.USER_ID, mUserID )
						.findAll ();
		if ( results.size () == 0 ) mListener.errorOnAcquiringUser ( "User not Found." );
		else if ( results.size () == 1 ) mListener.onUserAcquired ( results.get ( 0 ) );
		else mListener.errorOnAcquiringUser ( "Multiple Users Found." );
	}

	public boolean isInTransaction () {
		boolean isTransactionActive = false;
		if ( mRealm == null ) isTransactionActive = false;
		else {
			if ( mRealm.isClosed () ) isTransactionActive = false;
			else if ( mRealm.isInTransaction () ) isTransactionActive = true;
		} return isTransactionActive;
	}

	public void cancelTransaction () { mRealm.cancelTransaction (); }

	public interface GetUserListener {
		void onUserAcquired ( User user );
		void errorOnAcquiringUser ( String error );
	}
}