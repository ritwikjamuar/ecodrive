package io.recro.ecodrive.realmDBTasks;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.models.Trip;
import io.recro.ecodrive.models.User;

public class IsCyclingStartedTask {
	private IsCyclingStartedListener mListener;
	private Realm mRealm;
	private String mUserID;

	public IsCyclingStartedTask ( IsCyclingStartedListener mListener ) {
		this.mListener = mListener;
		onPreExecute ();
	}

	private void onPreExecute () { mRealm = Realm.getDefaultInstance (); }

	public void setData ( String mUserID ) { this.mUserID = mUserID; }

	public void execute () {
		RealmResults<User> results =
				mRealm
						.where ( User.class )
						.equalTo ( FieldConstants.USER_ID, mUserID )
						.findAll ();
		if ( results.size () == 0 ) mListener.errorOnFindingCyclingStarted ( "User not Found" );
		else if ( results.size () == 1 ) {
			User user = results.get ( 0 );
			if ( user == null ) mListener.errorOnFindingCyclingStarted ( "User data corrupted." );
			else {
				RealmList<Trip> trips = user.getTrip ();
				if ( trips == null ) mListener.errorOnFindingCyclingStarted ( "Trip Data Corrupted" );
				else {
					if ( trips.size () == 0 ) mListener.onCyclingNotStarted ();
					else {
						Trip trip = trips.last ();
						if ( trip == null ) mListener.errorOnFindingCyclingStarted ( "Trip Data Corrupted" );
						else {
							if ( trip.isTripStarted () ) mListener.onCyclingAlreadyStarted ( trip );
							else mListener.onCyclingNotStarted ();
						}
					}
				}
			}
		} else mListener.errorOnFindingCyclingStarted ( "Multiple Users Found" );
	}

	public boolean isInTransaction () {
		boolean isTransactionActive = false;
		if ( mRealm == null ) isTransactionActive = false;
		else {
			if ( mRealm.isClosed () ) isTransactionActive = false;
			else if ( mRealm.isInTransaction () ) isTransactionActive = true;
		} return isTransactionActive;
	}

	public void cancelTransaction () { mRealm.cancelTransaction (); }

	public interface IsCyclingStartedListener {
		void onCyclingAlreadyStarted ( Trip trip );
		void onCyclingNotStarted ();
		void errorOnFindingCyclingStarted ( String error );
	}
}