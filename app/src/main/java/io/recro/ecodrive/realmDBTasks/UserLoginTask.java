package io.recro.ecodrive.realmDBTasks;

import android.support.annotation.NonNull;

import io.realm.Realm;
import io.realm.RealmResults;

import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.models.User;

/** Represents an asynchronous login/registration task used to authenticate the user. */
public class UserLoginTask {
	private String mMobileNumber, mPassword;
	private UserLoginListener mListener;
	private Realm mRealm;
	private String mUserID;

	public UserLoginTask ( UserLoginListener mListener ) {
		this.mListener = mListener;
		onPreExecute ();
	}

	private void onPreExecute () { mRealm = Realm.getDefaultInstance (); }

	public void setFields ( String mMobileNumber, String mPassword ) {
		this.mMobileNumber = mMobileNumber;
		this.mPassword = mPassword;
	}

	public void execute () {
		RealmResults<User> results =
				mRealm
						.where ( User.class )
						.equalTo ( FieldConstants.MOBILE_NUMBER, mMobileNumber )
						.and ()
						.equalTo ( FieldConstants.PASSWORD, mPassword )
						.findAll ();
		if ( results.size () == 0 ) mListener.onLoginFailure ( "No Users Found" );
		else if ( results.size () == 1 ) mRealm.executeTransactionAsync ( transaction, onSuccess, onError );
		else mListener.onLoginFailure ( "Multiple Users Found" );
	}

	private Realm.Transaction transaction = new Realm.Transaction () {
		@Override public void execute ( @NonNull Realm realm ) {
			RealmResults<User> results =
					realm
							.where ( User.class )
							.equalTo ( FieldConstants.MOBILE_NUMBER, mMobileNumber )
							.and ()
							.equalTo ( FieldConstants.PASSWORD, mPassword )
							.findAll ();
			User user = results.get ( 0 );
			if ( user != null ) {
				mUserID = user.getId ();
				user.setLoggedIn ( true );
				realm.copyToRealmOrUpdate ( user );
			} else mListener.onLoginFailure ( "User Data Corrupted" );
		}
	};

	private Realm.Transaction.OnSuccess onSuccess = new Realm.Transaction.OnSuccess () {
		@Override public void onSuccess () {
			mListener.onLoginSuccess ( mUserID );
			mRealm.close ();
		}
	};

	private Realm.Transaction.OnError onError = new Realm.Transaction.OnError () {
		@Override public void onError ( @NonNull Throwable error ) {
			mListener.onLoginFailure ( "Login Error" );
			mRealm.close ();
		}
	};

	public boolean isTransactionOnGoing () {
		boolean isTransactionActive = false;
		if ( mRealm == null ) isTransactionActive = false;
		else {
			if ( mRealm.isClosed () ) isTransactionActive = false;
			else if ( mRealm.isInTransaction () ) isTransactionActive = true;
		} return isTransactionActive;
	}

	public void cancelTransaction () {
		if ( mRealm != null && mRealm.isInTransaction () ) mRealm.cancelTransaction ();
		mListener.onLoginFailure ( "Cancelled By User" );
	}

	public interface UserLoginListener {
		void onLoginSuccess ( String userID );
		void onLoginFailure ( String errorMessage );
	}
}