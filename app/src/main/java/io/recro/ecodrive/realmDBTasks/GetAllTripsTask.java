package io.recro.ecodrive.realmDBTasks;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.models.Trip;
import io.recro.ecodrive.models.User;

public class GetAllTripsTask {
	private GetAllTripsListener mListener;
	private Realm mRealm;
	private String mUserID;

	public GetAllTripsTask ( GetAllTripsListener mListener ) {
		this.mListener = mListener;
		onPreExecute ();
	}

	private void onPreExecute () { mRealm = Realm.getDefaultInstance (); }

	public void setData ( String mUserID ) { this.mUserID = mUserID; }

	public void execute () {
		RealmResults<User> results = mRealm.where ( User.class ).equalTo ( FieldConstants.USER_ID, mUserID ).findAll ();
		if ( results.size () == 0 ) mListener.errorOnFetchingAllTrips ( "No User Found" );
		else if ( results.size () == 1 ) {
			User user =  results.get ( 0 );
			if ( user == null ) mListener.errorOnFetchingAllTrips ( "User Data Corrupted" );
			else {
				RealmList<Trip> trips = user.getTrip ();
				if ( trips.size () == 0 ) mListener.onAllTripsFetched ( null );
				else mListener.onAllTripsFetched ( trips );
			}
		}
	}

	public boolean isInTransaction () {
		boolean isTransactionActive = false;
		if ( mRealm == null ) isTransactionActive = false;
		else {
			if ( mRealm.isClosed () ) isTransactionActive = false;
			else if ( mRealm.isInTransaction () ) isTransactionActive = true;
		} return isTransactionActive;
	}

	public void cancelTransaction () { mRealm.cancelTransaction (); }

	public interface GetAllTripsListener {
		void onAllTripsFetched ( RealmList<Trip> trips );
		void errorOnFetchingAllTrips ( String error );
	}
}