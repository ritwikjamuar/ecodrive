package io.recro.ecodrive.realmDBTasks;

import android.support.annotation.NonNull;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;
import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.models.User;

public class RegisterUserTask {
	private String userName, bikeId, mobileNumber, password; // Model fields to be added to the Realm.
	private RegisterUserListener mListener;
	private Realm mRealm; // Instance of Realm to perform transactions.
	private String mUserID;

	public RegisterUserTask ( RegisterUserListener mListener ) {
		this.mListener = mListener;
		onPreExecute ();
	}

	private void onPreExecute () {
		mRealm = Realm.getDefaultInstance ();
	}

	public void setFields ( String userName, String bikeId, String mobileNumber, String password ) {
		this.userName = userName;
		this.bikeId = bikeId;
		this.mobileNumber = mobileNumber;
		this.password = password;
	}

	public void execute () {
		android.util.Log.e ( "RegisterUser", "close ()" );
		RealmResults<User> results = mRealm
				.where ( User.class )
				.equalTo ( FieldConstants.USER_NAME, userName )
				.and ()
				.equalTo ( FieldConstants.BIKE_ID, bikeId )
				.and ()
				.equalTo ( FieldConstants.MOBILE_NUMBER, mobileNumber )
				.and ()
				.equalTo ( FieldConstants.PASSWORD, password )
				.findAll ();
		android.util.Log.e ( "results size", String.valueOf ( results.size () ) );
		if ( results.size () == 0 ) mRealm.executeTransactionAsync ( transaction, onSuccess, onError );
		else mListener.onUserAlreadyFound ();
	}

	private Realm.Transaction transaction = new Realm.Transaction () {
		@Override public void execute ( @NonNull Realm realm ) {
			mUserID = UUID.randomUUID ().toString ();
			User user = realm.createObject ( User.class, mUserID );
			user.setUserName ( userName );
			user.setBikeId ( bikeId );
			user.setMobileNumber ( mobileNumber );
			user.setPassword ( password );
			user.setLoggedIn ( true );
		}
	};

	private Realm.Transaction.OnSuccess onSuccess = new Realm.Transaction.OnSuccess () {
		@Override public void onSuccess () {
			mListener.onUserRegistered ( mUserID );
			mRealm.close ();
			android.util.Log.e ( "RegisterUser", "close ()" );
		}
	};

	private Realm.Transaction.OnError onError = new Realm.Transaction.OnError () {
		@Override public void onError ( @NonNull Throwable error ) {
			android.util.Log.e ( "Error", error.toString () );
			mListener.errorOnRegistration ( error.toString () );
			mRealm.close ();
			android.util.Log.e ( "RegisterUser", "close ()" );
		}
	};

	public boolean isTransactionOnGoing () {
		boolean isTransactionActive = false;
		if ( mRealm == null ) isTransactionActive = false;
		else {
			if ( mRealm.isClosed () ) isTransactionActive = false;
			else if ( mRealm.isInTransaction () ) isTransactionActive = true;
		} return isTransactionActive;
	}

	public void cancelTransaction () { mRealm.cancelTransaction (); }

	public interface RegisterUserListener {
		void onUserRegistered ( String userID );
		void onUserAlreadyFound ();
		void errorOnRegistration ( String error );
	}
}