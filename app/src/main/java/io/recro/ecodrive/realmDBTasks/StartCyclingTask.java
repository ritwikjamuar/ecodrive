package io.recro.ecodrive.realmDBTasks;

import android.support.annotation.NonNull;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.models.LatLng;
import io.recro.ecodrive.models.Trip;
import io.recro.ecodrive.models.User;

public class StartCyclingTask {
	private StartCyclingListener mListener;
	private Realm mRealm;
	private String userID;
	private LatLng mCurrentPosition;
	private long mStartTime;
	private String mTripID;

	public StartCyclingTask ( StartCyclingListener mListener ) {
		this.mListener = mListener;
		onPreExecute ();
	}

	private void onPreExecute () { mRealm = Realm.getDefaultInstance (); }

	public void setValues ( String userID, LatLng mCurrentPosition, long mStartTime ) {
		this.userID = userID;
		this.mCurrentPosition = mCurrentPosition;
		this.mStartTime = mStartTime;
	}

	public void execute () {
		RealmResults<User> results =
				mRealm
						.where ( User.class )
						.equalTo ( FieldConstants.USER_ID, userID )
						.findAll ();
		android.util.Log.e ( "results list", String.valueOf ( results.size () ) );
		if ( results.size () == 0 ) mListener.onCyclingStartError ( "User not found" );
		else if ( results.size () == 1 ) mRealm.executeTransactionAsync ( transaction, onSuccess, onError );
		else mListener.onCyclingStartError ( "Multiple Users Found" );
	}

	private Realm.Transaction transaction = new Realm.Transaction () {
		@Override public void execute ( @NonNull Realm realm ) {
			RealmResults<User> results =
					realm
							.where ( User.class )
							.equalTo ( FieldConstants.USER_ID, userID )
							.findAll ();
			User user = results.get ( 0 );
			if ( user == null ) mListener.onCyclingStartError ( "User data corrupted" );
			else {
				// Instantiate the new instance of Trip.
				mTripID = UUID.randomUUID ().toString ();
				Trip trip = realm.createObject ( Trip.class, mTripID );
				LatLng latLng = realm.createObject ( LatLng.class );
				latLng.setLatitude ( mCurrentPosition.getLatitude () );
				latLng.setLongitude ( mCurrentPosition.getLongitude () );
				trip.setTripStarted ( true );
				trip.setTripStartPosition ( latLng );
				trip.setStartTime ( mStartTime );

				// Add this instance of Trip to User Model.
				RealmList<Trip> trips = user.getTrip ();
				trips.add ( trip );
				user.setTrip ( trips );
				realm.copyToRealmOrUpdate ( user );
			}
		}
	};

	private Realm.Transaction.OnSuccess onSuccess = new Realm.Transaction.OnSuccess () {
		@Override public void onSuccess () {
			mListener.onCyclingStartSuccess ( mTripID );
			mRealm.close ();
		}
	};

	private Realm.Transaction.OnError onError = new Realm.Transaction.OnError () {
		@Override public void onError ( @NonNull Throwable error ) {
			mListener.onCyclingStartError ( error.toString () );
			mRealm.close ();
		}
	};

	public boolean isInTransaction () {
		boolean isTransactionActive = false;
		if ( mRealm == null ) isTransactionActive = false;
		else {
			if ( mRealm.isClosed () ) isTransactionActive = false;
			else if ( mRealm.isInTransaction () ) isTransactionActive = true;
		} return isTransactionActive;
	}

	public void cancelTransaction () { mRealm.cancelTransaction (); }

	public interface StartCyclingListener {
		void onCyclingStartSuccess ( String tripID );
		void onCyclingStartError ( String error );
	}
}