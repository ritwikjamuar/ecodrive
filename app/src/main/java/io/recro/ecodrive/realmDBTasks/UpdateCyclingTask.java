package io.recro.ecodrive.realmDBTasks;

import android.support.annotation.NonNull;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.models.LatLng;
import io.recro.ecodrive.models.Trip;
import io.recro.ecodrive.models.User;

public class UpdateCyclingTask {
	private UpdateCyclingListener mListener;
	private Realm mRealm;
	private LatLng mCurrentPosition;
	private double mDistance;
	private String mUserID;

	public UpdateCyclingTask ( UpdateCyclingListener mListener ) {
		this.mListener = mListener;
		onPreExecute ();
	}

	private void onPreExecute () { mRealm = Realm.getDefaultInstance (); }

	public void setValues ( String mUserID, LatLng mCurrentPosition, double mDistance ) {
		this.mCurrentPosition = mCurrentPosition;
		this.mDistance = mDistance;
		this.mUserID = mUserID;
	}

	public void execute () { mRealm.executeTransactionAsync ( transaction, onSuccess, onError ); }

	private Realm.Transaction transaction = new Realm.Transaction () {
		@Override public void execute ( @NonNull Realm realm ) {
			RealmResults<User> results =
					realm
							.where ( User.class )
							.equalTo ( FieldConstants.USER_ID, mUserID )
							.findAll ();
			if ( results.size () == 0 ) mListener.errorOnUpdatingCyclingTrip ( "User not found" );
			else if ( results.size () == 1 ) {
				User user = results.get ( 0 );
				if ( user == null ) mListener.errorOnUpdatingCyclingTrip ( "User Data Corrupted" );
				else {
					RealmList<Trip> trips = user.getTrip ();
					if ( trips == null ) mListener.errorOnUpdatingCyclingTrip ( "Trip Data Corrupted" );
					else {
						Trip trip = trips.last ();
						if ( trip == null ) mListener.errorOnUpdatingCyclingTrip ( "Trip Data Corrupted" );
						else {
							// Save Current Position.
							LatLng currentPosition = realm.createObject ( LatLng.class );
							currentPosition.setLatitude ( mCurrentPosition.getLatitude () );
							currentPosition.setLongitude ( mCurrentPosition.getLongitude () );
							trip.setTripLastKnownPosition ( currentPosition );

							// Save Distance.
							trip.setDistanceSaved ( mDistance );

							// Save Updated Trip Model to User.
							trips.set ( trips.size () - 1, trip );
							user.setTrip ( trips );

							// Save User.
							realm.copyToRealmOrUpdate ( user );
						}
					}
				}
			} else mListener.errorOnUpdatingCyclingTrip ( "Multiple Users Found" );
		}
	};

	private Realm.Transaction.OnSuccess onSuccess = new Realm.Transaction.OnSuccess () {
		@Override public void onSuccess () {
			mListener.onCyclingTripUpdated ();
			mRealm.close ();
		}
	};

	private Realm.Transaction.OnError onError = new Realm.Transaction.OnError () {
		@Override public void onError ( @NonNull Throwable error ) {
			mListener.errorOnUpdatingCyclingTrip ( error.toString () );
			mRealm.close ();
			android.util.Log.i ( "UpdateCyclingTask", "Error : " + error.toString () );
		}
	};

	public boolean isInTransaction () {
		boolean isTransactionActive = false;
		if ( mRealm == null ) isTransactionActive = false;
		else {
			if ( mRealm.isClosed () ) isTransactionActive = false;
			else if ( mRealm.isInTransaction () ) isTransactionActive = true;
		} return isTransactionActive;
	}

	public void cancelTransaction () { mRealm.cancelTransaction (); }

	public interface UpdateCyclingListener {
		void onCyclingTripUpdated ();
		void errorOnUpdatingCyclingTrip ( String error );
	}
}