package io.recro.ecodrive.realmDBTasks;

import io.realm.Realm;
import io.realm.RealmResults;
import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.models.User;

public class FindUserForPasswordForgottenTask {
	private FindUserForPasswordForgottenListener mListener;
	private Realm mRealm;
	private String mBikeID, mMobileNumber;

	public FindUserForPasswordForgottenTask ( FindUserForPasswordForgottenListener mListener ) {
		this.mListener = mListener;
		onPreExecute ();
	}

	private void onPreExecute () {
		mRealm = Realm.getDefaultInstance ();
	}

	public void setData ( String mBikeID, String mMobileNumber ) {
		this.mBikeID = mBikeID;
		this.mMobileNumber = mMobileNumber;
	}

	public void execute () {
		RealmResults<User> results =
				mRealm
						.where ( User.class )
						.equalTo ( FieldConstants.BIKE_ID, mBikeID )
						.and ()
						.equalTo ( FieldConstants.MOBILE_NUMBER, mMobileNumber )
						.findAll ();
		if ( results.size () == 0 ) mListener.errorOnFindingUser ( "User not found" );
		else if ( results.size () == 1 ) {
			User user = results.get ( 0 );
			if ( user == null ) mListener.errorOnFindingUser ( "User Data Corrupted" );
			else mListener.onUserFound ( user.getId () );
		} else mListener.errorOnFindingUser ( "Multiple Users Found" );
	}

	public boolean isInTransaction () {
		boolean isTransactionActive = false;
		if ( mRealm == null ) isTransactionActive = false;
		else {
			if ( mRealm.isClosed () ) isTransactionActive = false;
			else if ( mRealm.isInTransaction () ) isTransactionActive = true;
		} return isTransactionActive;
	}

	public void cancelTransaction () { mRealm.cancelTransaction (); }

	public interface FindUserForPasswordForgottenListener {
		void onUserFound ( String userID );
		void errorOnFindingUser ( String error );
	}
}