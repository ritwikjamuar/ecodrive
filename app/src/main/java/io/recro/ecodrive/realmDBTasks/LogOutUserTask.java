package io.recro.ecodrive.realmDBTasks;

import android.support.annotation.NonNull;

import io.realm.Realm;
import io.realm.RealmResults;

import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.models.User;

public class LogOutUserTask {
	private LogOutUserListener mListener;
	private Realm mRealm;
	private String mUserID;
	private RealmResults<User> mResults;

	public LogOutUserTask ( LogOutUserListener mListener ) {
		this.mListener = mListener;
		onPreExecute ();
	}

	private void onPreExecute () {
		mRealm = Realm.getDefaultInstance ();
	}

	public void setValues ( String mUserID ) {
		this.mUserID = mUserID;
	}

	public void execute () {
		android.util.Log.e ( "LogOutUser", "execute ()" );
		mResults =
				mRealm
						.where ( User.class )
						.equalTo ( FieldConstants.USER_ID, mUserID )
						.or ()
						.equalTo ( FieldConstants.IS_LOGGED_IN, true )
						.findAll ();
		if ( mResults.size () == 0 ) mListener.errorOnLoggingUserOut ( "User not found." );
		else if ( mResults.size () == 1 ) mRealm.executeTransactionAsync ( transaction, onSuccess, onError );
		else mListener.errorOnLoggingUserOut ( "Multiple Users Logged In." );
	}

	private Realm.Transaction transaction = new Realm.Transaction () {
		@Override public void execute ( @NonNull Realm realm ) {
			try {
				mResults =
						realm
								.where ( User.class )
								.equalTo ( FieldConstants.USER_ID, mUserID )
								.or ()
								.equalTo ( FieldConstants.IS_LOGGED_IN, true )
								.findAll ();
				User user = mResults.get ( 0 );
				if ( user != null ) {
					user.setLoggedIn ( false );
					realm.copyToRealmOrUpdate ( user );
				} else mListener.errorOnLoggingUserOut ( "User data corrupted" );
			} catch ( Exception e ) {
				e.printStackTrace ();
				mListener.errorOnLoggingUserOut ( "LogOut Error : " + e.toString () );
			}
		}
	};

	private Realm.Transaction.OnSuccess onSuccess = new Realm.Transaction.OnSuccess () {
		@Override public void onSuccess () {
			mListener.onUserLoggedOut ();
			mRealm.close ();
		}
	};

	private Realm.Transaction.OnError onError = new Realm.Transaction.OnError () {
		@Override public void onError ( @NonNull Throwable error ) {
			mListener.errorOnLoggingUserOut ( error.toString () );
			mRealm.close ();
		}
	};

	public boolean isInTransaction () {
		boolean isTransactionActive = false;
		if ( mRealm == null ) isTransactionActive = false;
		else {
			if ( mRealm.isClosed () ) isTransactionActive = false;
			else if ( mRealm.isInTransaction () ) isTransactionActive = true;
		} return isTransactionActive;
	}

	public void cancelTransaction () { mRealm.cancelTransaction (); }

	public interface LogOutUserListener {
		/**Indicates that user has been logged out successfully.*/
		void onUserLoggedOut ();

		/**<p>Indicates that Error has occurred while Logging out.</p>
		 * <p>Do note that this method can be invoked from Background thread,
		 * and running any code out of UI thread can cause Exception.</p>
		 * <p>So, while implementing this method, make sure you run the code on UI thread.</p>
		 * @param error String denoting cause of Error.*/
		void errorOnLoggingUserOut ( String error );
	}
}