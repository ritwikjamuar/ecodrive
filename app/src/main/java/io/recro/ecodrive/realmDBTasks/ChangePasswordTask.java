package io.recro.ecodrive.realmDBTasks;

import android.support.annotation.NonNull;

import io.realm.Realm;
import io.realm.RealmResults;
import io.recro.ecodrive.models.FieldConstants;
import io.recro.ecodrive.models.User;

public class ChangePasswordTask {
	private ChangePasswordListener mListener;
	private Realm mRealm;
	private String mUserID, mPassword;
	private boolean isPasswordChanged = false;

	public ChangePasswordTask ( ChangePasswordListener mListener ) {
		this.mListener = mListener;
		onPreExecute ();
	}

	private void onPreExecute () {
		mRealm = Realm.getDefaultInstance ();
	}

	public void setData ( String mUserID, String mPassword ) {
		this.mUserID = mUserID;
		this.mPassword = mPassword;
	}

	public void execute () {
		RealmResults<User> results =
				mRealm
						.where ( User.class )
						.equalTo ( FieldConstants.USER_ID, mUserID )
						.findAll ();
		if ( results.size () == 0 ) mListener.errorOnChangingPassword ( "User not Found" );
		else if ( results.size () == 1 ) mRealm.executeTransactionAsync ( transaction, onSuccess, onError );
		else mListener.errorOnChangingPassword ( "Multiple Users Found" );
	}

	private Realm.Transaction transaction = new Realm.Transaction () {
		@Override public void execute ( @NonNull Realm realm ) {
			RealmResults<User> results =
					realm
							.where ( User.class )
							.equalTo ( FieldConstants.USER_ID, mUserID )
							.findAll ();
			User user = results.get ( 0 );
			if ( user == null ) mListener.errorOnChangingPassword ( "User data corrupted" );
			else {
				String oldPassword = user.getPassword ();
				if ( mPassword.equals ( oldPassword ) ) mListener.errorOnChangingPassword ( "New Password is same as old password" );
				else {
					user.setPassword ( mPassword );
					realm.copyToRealmOrUpdate ( user );
					isPasswordChanged = true;
				}
			}
		}
	};

	private Realm.Transaction.OnSuccess onSuccess = new Realm.Transaction.OnSuccess () {
		@Override public void onSuccess () {
			if ( isPasswordChanged ) mListener.onPasswordChangedSuccessfully ();
			mRealm.close ();
		}
	};

	private Realm.Transaction.OnError onError = new Realm.Transaction.OnError () {
		@Override public void onError ( @NonNull Throwable error ) {
			mListener.errorOnChangingPassword ( error.toString () );
			mRealm.close ();
		}
	};

	public boolean isInTransaction () {
		boolean isTransactionActive = false;
		if ( mRealm == null ) isTransactionActive = false;
		else {
			if ( mRealm.isClosed () ) isTransactionActive = false;
			else if ( mRealm.isInTransaction () ) isTransactionActive = true;
		} return isTransactionActive;
	}

	public void cancelTransaction () { mRealm.cancelTransaction (); }

	public interface ChangePasswordListener {
		void onPasswordChangedSuccessfully ();
		void errorOnChangingPassword ( String error );
	}
}