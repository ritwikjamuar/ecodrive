package io.recro.ecodrive.realmDBTasks;

import io.realm.Realm;
import io.realm.RealmResults;

import io.recro.ecodrive.models.User;

public class FindLoggedInUserTask {
	private FindLoggedInListener mListener;
	private Realm mRealm;

	public FindLoggedInUserTask ( FindLoggedInListener mListener ) {
		this.mListener = mListener;
		onPreExecute ();
	}

	private void onPreExecute () { mRealm = Realm.getDefaultInstance (); }

	public void execute () {
		try {
			RealmResults<User> results =
					mRealm
							.where ( User.class )
							.equalTo ( "isLoggedIn", true )
							.findAll ();
			if ( results.size () == 0 ) mListener.onLoggedInUserNotFound ();
			else if ( results.size () == 1 ) {
				User user = results.get ( 0 );
				if ( user != null ) mListener.onLoggedInUserFound ( user.getId () );
				else mListener.errorOnFindingLoggedInUser ( "User is null" );
			} else mListener.errorOnFindingLoggedInUser ( "Too many Users" );
		} finally { mRealm.close (); /*Close the Realm as soon as Operation is done.*/ }
	}

	public boolean isInTransaction () {
		boolean isTransactionActive = false;
		if ( mRealm == null ) isTransactionActive = false;
		else {
			if ( mRealm.isClosed () ) isTransactionActive = false;
			else if ( mRealm.isInTransaction () ) isTransactionActive = true;
		} return isTransactionActive;
	}

	public void cancelTransaction () { mRealm.cancelTransaction (); }

	public interface FindLoggedInListener {
		void onLoggedInUserFound ( String userID );
		void onLoggedInUserNotFound ();
		void errorOnFindingLoggedInUser ( String error );
	}
}